#
# Base Docker image containing the software - NOT to be used at runtime!
# This Dockefile builds the software only!
#
# docker build --target=builder-ci --build-arg GIT_BRANCH=v0.0.4 -t glass:v0.0.4-base --rm=false --pull --network host -f=Dockerfile .
#
FROM node:18-alpine as builder-ci

RUN echo "Building docker image in node $(node -v) and npm $(npm -v)"

# The node environment:
# This is the most important one, as it affects NPM described below.
# In short NODE_ENV=production switch middlewares and dependencies to efficient code path and NPM installs only packages in dependencies.
# Packages in devDependencies and peerDependencies are ignored.
ARG NODE_ENV

ENV NODE_ENV=${NODE_ENV:-development}

ENV TARGET_FOLDER_NAME="demonstrator-backoffice"

RUN echo "Building image under node $(node -v) and npm $(npm -v)"

RUN apk --no-cache add git && apk add --no-cache --upgrade bash

RUN npm install -g node-gyp

# create folder glass-wallet-workspace
RUN mkdir -p ./$TARGET_FOLDER_NAME
RUN mkdir -p ./$TARGET_FOLDER_NAME/apihub-root


COPY apihub-root/external-volume/config ./$TARGET_FOLDER_NAME/apihub-root/external-volume/config
COPY env.json ./$TARGET_FOLDER_NAME/

# demonstrator assets
COPY package*.json .npmrc octopus-freeze.json ./$TARGET_FOLDER_NAME/

# backoffice assets
COPY backoffice/package*.json backoffice/.npmrc backoffice/tsconfig.json backoffice/gulpfile.js ./$TARGET_FOLDER_NAME/backoffice/
COPY backoffice/src ./$TARGET_FOLDER_NAME/backoffice/src
COPY backoffice/assets ./$TARGET_FOLDER_NAME/backoffice/assets

RUN echo "Environment suffix is ${ENV_SUFFIX}"

ARG CONFIG_FOLDER
ENV CONFIG_FOLDER=${CONFIG_FOLDER:-config}

RUN echo "Config folder is ${CONFIG_FOLDER}"

# copy the generic configurations that will then be converted to domain specific config and override the ones in the repository
COPY docker/${CONFIG_FOLDER} ./$TARGET_FOLDER_NAME/docker/config

# copy the commands that will be ran by the boot.js script
COPY docker/replace.sh ./$TARGET_FOLDER_NAME/replace.sh
RUN chmod u+x ./$TARGET_FOLDER_NAME/replace.sh

# Install the dependencies from package-lock.json
RUN cd $TARGET_FOLDER_NAME && npm install && npm cache clean --force

COPY trust-loader-config ./$TARGET_FOLDER_NAME/trust-loader-config
COPY patch ./$TARGET_FOLDER_NAME/patch
#
## test resources
#COPY tests ./$TARGET_FOLDER_NAME/tests
#COPY jest.config.js ./$TARGET_FOLDER_NAME/jest.config.js

# Make the workfolder available to the the 'node' user. The `node` user is built in the Node image.
RUN cd $TARGET_FOLDER_NAME && chown -R node:node .

# make it run as user node
USER node

WORKDIR $TARGET_FOLDER_NAME

RUN echo "Market Domain is ${MARKET_DOMAIN}"

ENV GLASS_DOMAIN="glass${ENV_SUFFIX}"

# ARG DOMAIN
ENV DOMAIN=${DOMAIN:-$GLASS_DOMAIN}
RUN echo "Domain is ${DOMAIN}"

ENV VAULT_DOMAIN=${VAULT_DOMAIN:-$GLASS_DOMAIN"-vault"}
RUN echo "Vault Domain is ${VAULT_DOMAIN}"

ENV DID_DOMAIN="${VAULT_DOMAIN}"
RUN echo "DID Domain is ${DID_DOMAIN}"

ARG ENV_SUFFIX
ENV ENV_SUFFIX=${ENV_SUFFIX}
RUN echo "Environment suffix is ${ENV_SUFFIX}"

ARG ENV_PREFFIX
ENV ENV_PREFFIX=${ENV_PREFFIX}
RUN echo "Environment Preffix is ${ENV_PREFFIX}"

ENV ENV_SUFFIX_FOR_REPLACE=${ENV_SUFFIX:-"\"\""}

ARG ADAPTER_ENDPOINT
ENV ADAPTER_ENDPOINT=${ADAPTER_ENDPOINT:-localhost:3000}
RUN echo "Adapter endpoint is ${ADAPTER_ENDPOINT}"

ARG GLASS_ENV
ENV GLASS_ENV=${GLASS_ENV:-local}

ARG COUNTRY_CODE
ENV COUNTRY_CODE=${GLASS_COUNTRY_CODE:-pt}

ARG EGOV_REST
ENV EGOV_REST=${EGOV_REST:-http://localhost:3000}

ARG EGOVID_REST
ENV EGOVID_REST=${EGOVID_REST:-http://localhost:3001}

ARG EGOVPASSPORT_REST
ENV EGOVPASSPORT_REST=${EGOVPASSPORT_REST:-http://localhost:3002}

ARG EGOVTAX_REST
ENV EGOVTAX_REST=${EGOVTAX_REST:-http://localhost:3003}

ARG EGOVHEALTH_REST
ENV EGOVHEALTH_REST=${EGOVHEALTH_REST:-http://localhost:3004}

ARG DISABILITY_REST
ENV DISABILITY_REST=${DISABILITY_REST:-http://localhost:3004}

ARG EGOVEDUCATION_REST
ENV EGOVEDUCATION_REST=${EGOVEDUCATION_REST:-http://localhost:3005}

ARG BANK_REST
ENV BANK_REST=${BANK_REST:-http://localhost:8081}

ARG PDMFC_REST
ENV PDMFC_REST=${PDMFC_REST:-http://localhost:8081}

ARG LANDLORD_REST
ENV LANDLORD_REST=${LANDLORD_REST:-http://localhost:8081}

ARG GLASS_REST
ENV GLASS_REST=${GLASS_REST:-http://localhost:8080}

ARG GlassMiddleware
ENV GlassMiddleware=${GlassMiddleware:-bank}

ARG LOCALE_KEY
ENV LOCALE_KEY=${LOCALE_KEY:-pt}

# Override the demonstrator environment Configs
RUN node ./node_modules/@glass-project1/dsu-utils/src/streamAndReplace.js --srcPath="./trust-loader-config" --paramIdentifier='%' --destPath="./apihub-root/" %GLASS_REST=${GLASS_REST} %EGOVID_REST=${EGOVID_REST} %EGOVPASSPORT_REST=${EGOVPASSPORT_REST} %EGOV_REST=${EGOV_REST} %EGOVTAX_REST=${EGOVTAX_REST}  %EGOVEDUCATION_REST=${EGOVEDUCATION_REST}  %EGOVHEALTH_REST=${EGOVHEALTH_REST}  %DISABILITY_REST=${DISABILITY_REST} %BANK_REST=${BANK_REST} %PDMFC_REST=${PDMFC_REST} %LANDLORD_REST=${LANDLORD_REST}


# Override the Apihub Configs
# RUN node ./node_modules/@glass-project1/dsu-utils/src/streamAndReplace.js --srcPath=./docker/$CONFIG_FOLDER --paramIdentifier='%' --destPath=./apihub-root/external-volume// --baseFolderReplacement="config" %ENV_PREFFIX="$ENV_PREFFIX" %ENV_SUFFIX="$ENV_SUFFIX_FOR_REPLACE" %ADAPTER_ENDPOINT=$ADAPTER_ENDPOINT %MIDDLEWARE=$GlassMiddleware %COUNTRY_CODE=$COUNTRY_CODE
 
# RUN echo $(cat apihub-root/bdns.hosts)   
# RUN echo $(cat apihub-root/external-volume/config/bdns.hosts) 

# RUN echo $(cat apihub-root/apihub.json)  
# RUN echo $(cat apihub-root/external-volume/config/apihub.json) 


RUN echo "Glass environment is ${GLASS_ENV}"

# Start the server and run build-all
RUN echo "Preparing to build demonstrator $DOMAIN"
RUN cd backoffice && npm run build:prod

ARG VERSION
ENV VERSION=${VERSION:-local}

# clean up
RUN rm -rf ./trust-loader-config ./patch


## -----------------------------------------------------------------------------------
##
# Used for runtime. This is a minimal image based on Alpine.
# We copy the build result from "base" image to this image
#
# Build:
# docker build --target=runtime-ci --build-arg GIT_BRANCH=v0.0.4 -t glass:v0.0.4 --rm=false --pull --network host -f=Dockerfile .
#
# Run:
# docker run --rm fgt:v0.8.6
FROM node:18-alpine as runtime

# The node environment:
# This is the most important one, as it affects NPM described below.
# In short NODE_ENV=production switch middlewares and dependencies to efficient code path and NPM installs only packages in dependencies.
# Packages in devDependencies and peerDependencies are ignored.
ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV:-production}

ENV TARGET_FOLDER_NAME="demonstrator-backoffice"

RUN echo "Building image under node $(node -v) and npm $(npm -v)"

RUN apk --no-cache add vim htop less grep ca-certificates && apk add --no-cache --upgrade bash

RUN npm install -g node-gyp

COPY --chown=node:node --from=builder-ci /$TARGET_FOLDER_NAME /$TARGET_FOLDER_NAME

WORKDIR /$TARGET_FOLDER_NAME/privatesky

ARG CONFIG_FOLDER
ENV CONFIG_FOLDER=${CONFIG_FOLDER:-config}

ARG ENV_SUFFIX
ENV ENV_SUFFIX=${ENV_SUFFIX}
RUN echo "Environment suffix is ${ENV_SUFFIX}"

ARG ENV_PREFFIX
ENV ENV_PREFFIX=${ENV_PREFFIX}
RUN echo "Environment Preffix is ${ENV_PREFFIX}"

ARG COUNTRY_CODE
ENV COUNTRY_CODE=${GLASS_COUNTRY_CODE:-pt}
RUN echo "Country code is ${COUNTRY_CODE}"

ARG ADAPTER_ENDPOINT
ENV ADAPTER_ENDPOINT=${ADAPTER_ENDPOINT:-localhost:3000}
RUN echo "Adapter endpoint is ${ADAPTER_ENDPOINT}"

ARG COUNTRY_CODE
ENV COUNTRY_CODE=${GLASS_COUNTRY_CODE:-pt}

# ARG EGOVID_REST
# ENV EGOVID_REST=${EGOVID_REST:-http://localhost:3001}

# ARG EGOVPASSPORT_REST
# ENV EGOVPASSPORT_REST=${EGOVPASSPORT_REST:-http://localhost:3002}

# ARG EGOVTAX_REST
# ENV EGOVTAX_REST=${EGOVTAX_REST:-http://localhost:3003}

# ARG EGOVHEALTH_REST
# ENV EGOVHEALTH_REST=${EGOVHEALTH_REST:-http://localhost:3004}

# ARG DISABILITY_REST
# ENV DISABILITY_REST=${DISABILITY_REST:-http://localhost:3004}

# ARG EGOVEDUCATION_REST
# ENV EGOVEDUCATION_REST=${EGOVEDUCATION_REST:-http://localhost:3005}

# ARG BANK_REST
# ENV BANK_REST=${BANK_REST:-http://localhost:8081}

# ARG PDMFC_REST
# ENV PDMFC_REST=${PDMFC_REST:-http://localhost:8081}

# ARG LANDLORD_REST
# ENV LANDLORD_REST=${LANDLORD_REST:-http://localhost:8081}

ARG GlassMiddleware
ENV GlassMiddleware=${GlassMiddleware:-bank}

ARG LOCALE_KEY
ENV LOCALE_KEY=${LOCALE_KEY:-pt}

#ENV MARKET_DOMAIN="glass${ENV_SUFFIX:-.local}"

RUN echo "Market Domain is ${MARKET_DOMAIN}"

ENV GLASS_DOMAIN="glass${ENV_SUFFIX}"

ARG DOMAIN
ENV DOMAIN=${DOMAIN:-$GLASS_DOMAIN}
RUN echo "Domain is ${DOMAIN}"

ENV VAULT_DOMAIN=${VAULT_DOMAIN:-$GLASS_DOMAIN"-vault"}
RUN echo "Vault Domain is ${VAULT_DOMAIN}"

ENV DID_DOMAIN="${VAULT_DOMAIN}"
RUN echo "DID Domain is ${DID_DOMAIN}"

ARG GLASS_REST
ENV GLASS_ENDPOINT=${GLASS_REST:-http://localhost:8080}

ARG GLASS_REST
ENV GLASS_REST=${GLASS_REST:-http://localhost:8080}

# APIHub
EXPOSE 8080/tcp

# sets Apihub's environment variables
ENV DEV=false
ENV PSK_CONFIG_LOCATION="../apihub-root/external-volume/config"
ENV PSK_TMP_WORKING_DIR="tmp"
ENV OPENDSU_ENABLE_DEBUG=true

WORKDIR /$TARGET_FOLDER_NAME

# Start the ApiHub via the node direct call
CMD ["sh", "-c", "./replace.sh"]