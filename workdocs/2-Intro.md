## Demonstrator-Backoffice

This repository acts as the demonstrator to interact and experience the full functionallity of the Glass-Wallet.

Node versions are documented in glass wallet workspace, but at the time of this readme are:
- Node = 16;
- NPM >= 8.0.0; (we have not fully tested with npm above 8.10 but in principle it shouldn't matter)

#include "./workdocs/tutorials/01-Installation.md"

