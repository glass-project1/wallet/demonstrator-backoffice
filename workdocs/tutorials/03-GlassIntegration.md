<!-- ### Glass Integration

#### Dependencies
    "@glass-project1/logging": "latest",
    "@glass-project1/decorator-validation": "latest",
    "@glass-project1/db-decorators": "latest",
    "@glass-project1/opendsu-types": "latest",
    "@glass-project1/dsu-blueprint": "latest",
    "@glass-project1/ui-decorators": "latest",         
    "@glass-project1/glass-toolkit": "latest",
    "@glass-project1/native-integration": "latest" <-- only needed if you require acces to native features (camera, localization, etc)

The above dependencies are 'boilerplate' for glass related integrations (when regarding the wallet and opendsu)

    "@glass-project1/dsu-utils": "latest",

This is used by the build scripts and environment setup

    "@glass-project1/base-web-components": "latest",   <-- common use web components
    "@glass-project1/localization": "latest",          <-- UI localization lib  
    "@glass-project1/ui-decorators-web": "latest",     <-- adds rendering strategies to blueprints (eg: the model can render itself in HTML5) 

These are UI dependencies, which you can use or not. (if you do, you'll also need ```@ionic/core```)

#### Wallet API interactions

For the most part, all the objects you need to interact with your instance of the DApp (database, did, etc) are supplied in ```glass-toolkit```. 
Please refer to the [documentation](https://glass-project1.gitlabpages.ubitech.eu/wallet/glass-toolkit/) 

##### Updating glass related dependencies in bulk

We use package-lock.json to freeze our dependencies. To update them in bulk the recommended procedure is to delete the ```@glass-project1``` folder from the ```node_modules```
as well as ```package-lock.json``` and run ```npm install```. This way all no glass dependencies will remain the same.

#### Warnings during build

If you are using ```@glass-project1/base-web-components``` and/or ```@glass-project1/native-integration``` then the following warnings might show up during stencils build process:

```
[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/clipboard' is imported by
           ./node_modules/@glass-project1/native-integration/lib/clipboard/clipboard.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/filesystem' is imported by
           ./node_modules/@glass-project1/native-integration/lib/fs-storage/FsStorage.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor-community/camera-preview' is imported by
           ./node_modules/@glass-project1/native-integration/lib/camera/camera.js, but could not be resolved – treating
           it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/device' is imported by ./node_modules/@glass-project1/native-integration/lib/device/device.js,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/geolocation' is imported by
           ./node_modules/@glass-project1/native-integration/lib/location/location.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/storage' is imported by
           ./node_modules/@glass-project1/native-integration/lib/light-storage/storage.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/haptics' is imported by ./node_modules/@glass-project1/native-integration/lib/haptics/haptics.js,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/clipboard' is imported by @capacitor/clipboard?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/filesystem' is imported by @capacitor/filesystem?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor-community/camera-preview' is imported by @capacitor-community/camera-preview?commonjs-external,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/device' is imported by @capacitor/device?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/geolocation' is imported by @capacitor/geolocation?commonjs-external, but could not be resolved
           – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/storage' is imported by @capacitor/storage?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/haptics' is imported by @capacitor/haptics?commonjs-external, but could not be resolved –
           treating it as an external dependency
```

That is ok. These dependencies will be provided to you by the wallet/loader. Since they'll require native code, they have to be builtin the loader. They cannot be loaded at runtime as per app store regulations;

You will also have these warnings:

```
[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           'opendsu' is imported by ./node_modules/@glass-project1/opendsu-types/lib/opendsu.js, but could not be
           resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           'opendsu' is imported by opendsu?commonjs-external, but could not be resolved – treating it as an external
           dependency
```

This is also ok. OpenDSU will be injected into you DApp by the wallet/loader. Since it is not available on npm, it does not follow normal dependency rules



 -->
