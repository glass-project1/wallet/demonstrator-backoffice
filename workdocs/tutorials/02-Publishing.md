<!-- ### Creating releases and Publishing to the DApp Market

#### Releases

under this repository (for linux and macOS), you can run ```npm run release``` and a small script will:
 - show you all existing tags
 - ask you to choose and confirm a new tag
 - ask you to choose and confirm a tagging message
 - run `npm run build:prod`
 - run `npm run test`
 - create the tag, and push it to the remote repository

***NOTE:*** the tag creation and pushing is not affected by the results of the build process or the tests.
This is because, since the actual building happens in CI, we sometimes have situations where we want to continue the process regardless of these results locally

Optionally you can do it manually by simply creating a tag and pushing it to the remote

#### Publishing

##### To NPM package registry

Our CI is programmed to, whenever you create a tag in the master branch, automatically run the build process and publish is to NPM.


notice the publishing config entry in your package.json. The project ID must the one for the repository you are using. you can find it under the repository name in gitlab's page

```
  "publishConfig": {
    "@glass-project1:registry": " https://gitlab.com/api/v4/projects/890/packages/npm/"
  },
```

##### To the DApp Market

While we do our development in dedicated branches that are merged to the ```master``` branch, we also maintain 2 special branches in our repositories:
 - ```dev```: controls our dev release;
 - ```tst```: controls ours tst release

DApp publishing is set to automatically happen when we push to these branches via CI


###### via CI

Apart from local development and testing, we rely heavily on CI/CD for certain operations:
 - Continuous testing of all repositories;
 - Automatic library release to the gitlab npm registry;
 - Automatic docker build and publish to the gitlab docker registry;
 - Automatic DApp publishing to the Glass DApp Market;

For the sake of this readme, only the latter is relevant.

The ```.gitlab-ci.yml``` file defines certain operations:

 - before_script: 
   - updates os dependencies, install the necessary ones to run and test stencil apps and caches them;
   - installs npm dependencies from package-lock.json and caches them;
 - tests: basically runs the command ```npm run test:ci -- --command="npm run test:all"``` which runs all tests with open dsu in the process to allow for integrations tests
 - build: basically runs the build:prod command;
 - npm-deploy: publishes the package to npm when a tag is created
 - dsu-publish: builds the application and publishes it to the running DApp market on pdm servers ***if the branch is 'dev' or 'tst'***. This allows us to work in the master branch, tag the master branch, and only merge those tags to dev or tst branches, triggering the dsu publication

###### Manually

Having the server running and after having built your application (including the dsu) run ```npm run publish:dsu -- --pathToOpenDSU='./privatesky/psknode/bundles/openDSU.js'```;

this will publish the app in you running local instance of the dapp-market (domain is 'glass.local') you can change any of the settings by overriding the parameters passed in the original npm script

###### Automatic publish on environment reset

Since we are in development, sometimes it will be required to reset the dev and tst environments.

When that happens we have set up our ```glass-wallet-workspace``` CI in a way we can quickly re-publish all the dapps required for the wallet to fully function.

This translates to a CI script found under the ```glass-wallet-workspace``` repository you can find [here](https://gitlab.com/glass-project1/wallet/glass-wallet-workspace/-/blob/master/dapp-publish.yml)

If you want your dapp to be automatically published when we reset our environments, please do a Pull Request adding your dapp to this file, and give the PDM&FC team permissions to trigger your repository's CI -->