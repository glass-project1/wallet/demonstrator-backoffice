### Installation

In order to use the demonstrator-backoffice you need to follow a list of steps presented below:

#### Step 1: Installation

Run ```npm install``` - installs the necessary dependencies

#### Step 2: Developing

This project is divided in two parts the backend and frontend. The frontend is a stencil/ionic project, the source code is under the ```src``` folder. For more details on how to develop Stencil Applications, 
please visit the [stencil website](https://stenciljs.com/).  
The backend is a typescript project that contains small middlewares that will run on the apiHub server, the source code for the backend is located under the ```backoffice/src```.
<!-- 

variables 
MARKET_DOMAIN=glass.local GlassMiddleware=moj LOCALE_KEY=pt npm run server

#### Step 3: Building -->

<!-- This application is set up so it can be built under an ```OpenDSU Workspace``` (examples can be found [here](https://gitlab.com/glass-project1/wallet/templates/opendsu-workspace-template), [here](https://gitlab.com/glass-project1/wallet/glass-wallet-workspace), [here](https://gitlab.com/glass-project1/wallet/workspaces/dsu-explorer-workspace), and [here](https://gitlab.com/glass-project1/wallet/dsu-blueprint-workspace)).

To build and test it in a standalone fashion (the same way that is done in CI):

```npm run test:ci -- --command="npm run build:prod"``` - This will:
 - emulate an OpenDSU workspace under the current repository:
   - Add an ```apihub-root``` folder, complete with all the necessary configuration files;
   - add some dependencies and new scripts to ```package.json``` (one of them is OpenDSU's taskrunner ```octopus```);
   - start the OpenDSU Apihub (equivalent to the ```npm run server``` command)
   - add an ```octopus``` configuration file ```octopus-freeze.json``` than contains the tasks necessary for:
     - Cloning the version controlled OpenDSU repository and perform the necessary build procedures;
     - Cloning the ```blueprint-loader``` repository, building it and deploy it to the apihub, allowing you to test this template application in the browser;
 - re-run ```npm install```, which will this time also install the dependencies in the ```octopus-freeze``` file
 - start the OpenDSU ApiHub server (basically run ```npm run server```)
 - perform the build command ```npm run build-all``` that will instruct ```octopus``` to run its config file ```octopus-freeze.json```;
 - finally, run the ```npm run build:prod``` command which will pack you application under the ```www``` folder;
 - ***IMPORTANT*** the ci command adds several folders and modifies several files (like package.json and stencil.config.json). ***These changes should not be committed!!***

***Note:*** After the last command, since it's designed to run in CI, some lingering processes might remain that can impede the running of the server.
To prevent that locally you can ```kill -9 $(pgrep node)``` in linux. In windows all the lingering node processes must be stopped -->
<!-- 
#### Step 3.1: Rebuilding the Application
 
After you have the ```npm run test:ci``` command (and killed any lingering processes), to rebuild the application, the steps are easier:
 - manually start the OpenDSU ApiHub server: ```npm run server```;
 - on a separate terminal:
   - build the application: ```npm run build:prod``` or ```npm run build``` (the difference in this case lies in Stencil's build process. build is for the dev version while prod packs the application in a minified and optimized fashion). This will rebuild your ```www``` folder;
   
#### Step 4: Packing the Application in DSU format

##### For the first time

If you ran the ```npm run test:ci -- --command="npm run npm run build:prod"``` above, then the application will already have been packed in DSU format:
 - you will find a ```seed``` file under the root folder containing the KeySSI for the DSU where the Application code is stored;
 - under ```./apihub-root/dapp-template/loader``` you will find the code to a simple loader web app, that can register new instances of the ```dapp-template``` and launch them via a simple register/login procedure. it will be available under ```localhost:8080/dapp-template/loader```;
 - under ```./apihub-root/dapp-template/wallet-patch``` you will find a ```seed``` file, which will contain the same KeySSI as the previous ```seed``` file

To replicate this 'manually', make sure the server is running and run ```npm run build-all```

##### For subsequent builds

 - make sure the server is running;
 - make sure you have already built your application and have a ```www``` folder, or rebuild it via the ```npm run build``` or ```npm run build:prod``` commands
 - run the command ```npm run build:dsu```. You should be an output containing the generated KeySSI;
 - Since this is an update, the same DSU will be used, and any subsequent login to an already existing dapp instance will automatically display the updated code. (since we use service workers, for development, it's usually better to use incognito window, and close/reopen them in between builds)

#### Technical Details

##### Octopus

Octopus is a taskrunner, that reads from a json file ```octopus-freeze.json``` and executes the commands defined in that file.

For example, when you run ```npm install``` it will also execute all the commands in the ```dependencies``` entry. And when you run ```npm run build-all``` it will executed the commands in the ```build``` entry

##### Blueprints

Under the ```./blueprint``` folder you will find a ```build.ts``` file containing the definition to a Type script class. This very simple class has the definition on how to create the DSU.
In this case:

```ts
@DSUBlueprint(undefined)                                                    // <-- Defines the class as a DSU, undefined means hte domain where it is build can be selected by the build process, and the KeySSI type default to SeedSSI
export default class WalletBuildBlueprint extends DBModel {

    @addFolderFS('www', '/')                                                // <-- Instructs the dsu-bluprint library to copy the contents of the www folder onto the root of the dsu
    code?: any = undefined;

    constructor(blueprint?: WalletBuildBlueprint | {}) {
        super();
        constructFromObject<WalletBuildBlueprint>(this, blueprint);
    }
}
```

The ```dsu-blueprint``` library is the CORE component to all the functionalities we are presenting. For more details please review the documentation and tests. We use them for:
 - Simple DSU definition: (files, folders, simple data);
 - validation: there are several validation decorators you can apply (and create new ones)
 - rendering: instructing a model on how to render itself (you can see an example of this in the ```glass-loader```)

When you execute ```npm run build``` or ```npm run build:prod``` these blueprints will be transpiled and exported to the `./build` folder to be later used by the ```npm run build:dsu``` command;

##### Stencil

The ```npm run build``` or ```npm run build:prod``` commands trigger Stencil's build process. The definitions for which can be found under ```stencil.config.js```.

Most of this file is boilerplate, but here are some notable points:
 - namespace: controls the name of the output files (these must match the ones in package.json);
 - globalStyle and globalScript: self-descriptive. global script will be executed on boot of the application, global styles sets a global style for the application;
 - rollupPlugins: 
   - before: We have a trigger to run gulp. more on this in the next chapter;
   - after: nodePolyFills: we need this for the wallet because we use the Buffer 'node' object for encryption and we need it polyfilled. You might not need this.
 - outputTargets: the one that interests us is the 'www'. notice how we copy the required dependencies to the www folder during build.

###### Testing

The testing follows stencil test procedures, eg: ts files containing the keywords 'spec' and 'e2e'.

To these stencil specific keywords we added the 'integration' keyword, that will be ran with open dsu in its environment.

This allows for integration testing with any wallet/opendsu features

##### Gulp

In conjunction with stencil's build process we use gulp to:
 - transpile the blueprints to standard javascript;
 - remove the hardcoded OpenDSU requires stencil leaves behind, that would result in an error. This dependency is later injected by our loader code;
 - hardcode the current version onto the code
 - remove a service worker script that stencil leaves behind in dev builds that conflicts with ours; -->