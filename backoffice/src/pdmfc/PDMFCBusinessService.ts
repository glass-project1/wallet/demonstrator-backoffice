import { GlassEvidenceRequest, HTTP_METHODS } from "@glass-project1/glass-toolkit";
import { EmployerActions } from "@glass-project1/glass-toolkit/lib/providers";
import { Callback, errorCallback } from "@glass-project1/logging";
import { IncomingMessage } from "http";
import { AbsBusinessService } from "../general/AbsBusinessService";
import { RouteDefinitions } from "../utils";

const BankBusinessRoutes: RouteDefinitions = {
    "handlePDMFCWorkContractIssue" : {
        path: "/work-contract",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class PDMFCBusinessService extends AbsBusinessService {
    constructor(server: any) {
        super(server, BankBusinessRoutes)
    }

    protected handlePDMFCWorkContractIssue(data: {did: string}, req: IncomingMessage, callback: Callback){
        const self : PDMFCBusinessService = this;

        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);
       
        const evidenceRequest = new GlassEvidenceRequest({
            requestId: Date.now().toString(),
            purpose: this.locale.get("purposes.issue.work-contract") as string,
            action: EmployerActions.ISSUE_WORK_CONTRACT,
            requesterDID: self.getDID().getIdentifier(),
            receiverDID: did,
            payload: ["egov.*.moj.id.*"]
        })

        self.walletManager.handleRequest(evidenceRequest)
            .then((result: any) =>{callback(undefined, result)})
            .catch((e: any) => {errorCallback.call(self, e, callback)})
    }
}