import { getInjectablesRegistry } from "@glass-project1/db-decorators"
import { EGovServiceWallet, MessagingHub } from "@glass-project1/glass-toolkit"
import { ApiHubService, apiHubServiceInitializer, bootEmployerWallet } from "@glass-project1/glass-toolkit/lib/providers"
import { BackendFileService } from "@glass-project1/glass-toolkit/lib/providers/BackendFileService"
import { Callback, Err, errorCallback } from "@glass-project1/logging"
import { EvidenceService } from "../general/EvidenceService"
import { BootFunction, InitFunction, MiddlewareEnvironmentDefinition } from "../utils"
import { PDMFCBusinessService } from "./PDMFCBusinessService"

export const init: InitFunction = (server: any, environment: MiddlewareEnvironmentDefinition, callback: Callback) => {
    try {
        apiHubServiceInitializer(server, [
            EvidenceService,
            PDMFCBusinessService as unknown as ApiHubService
        ])
    } catch (e: any){
        errorCallback("Middleware failed: {0}", callback, e.message || e)
    }
}

export const boot: BootFunction = function(blueprint: EGovServiceWallet, fileService: BackendFileService, init: boolean, callback: Callback){

    bootEmployerWallet(blueprint, fileService, init, (err: Err) => {
        if (err)
            return errorCallback.call(boot, err || "Missing manager", callback);

        const hub: MessagingHub = getInjectablesRegistry().get("MessageHub") as MessagingHub;
        if (!hub)
            return errorCallback.call(boot, "Missing Message hub", callback);
        (hub as unknown as {startListening: () => void}).startListening();
        callback();
    });
}