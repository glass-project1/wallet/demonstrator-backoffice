
import { GlassEvidenceRequest, HTTP_METHODS } from "@glass-project1/glass-toolkit";
import { BankActions } from "@glass-project1/glass-toolkit/lib/providers";
import { Callback, errorCallback } from "@glass-project1/logging";
import { IncomingMessage } from "http";
import { AbsBusinessService } from "../general/AbsBusinessService";
import { RouteDefinitions } from "../utils";


const BankBusinessRoutes: RouteDefinitions = {
    "handleBankAccountProof" : {
        path: "/bank",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class BankBusinessService extends AbsBusinessService {
    constructor(server: any) {
        super(server, BankBusinessRoutes)
    }

    protected handleBankAccountProof(data: {did: string}, req: IncomingMessage, callback: Callback){
        const self : BankBusinessService = this;

        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);
       
        const evidenceRequest = new GlassEvidenceRequest({
            requestId: Date.now().toString(),
            purpose: this.locale.get("purposes.issue.proof") as string,
            action: BankActions.ISSUE_BANK_ACCOUNT,
            requesterDID: self.getDID().getIdentifier(),
            receiverDID: did,
            payload: ["egov.*.moj.id.*", "egov.*.mof.tax.*"]
        })

        self.walletManager.handleRequest(evidenceRequest)
            .then((result: any) =>{callback(undefined, result)})
            .catch((e: any) => {errorCallback.call(self, e, callback)})
    }
}