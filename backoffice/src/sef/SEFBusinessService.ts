import {GlassEvidenceRequest, HTTP_METHODS} from "@glass-project1/glass-toolkit";
import {RouteDefinitions} from "../utils";
import {AbsBusinessService} from "../general/AbsBusinessService";
import {Callback, errorCallback} from "@glass-project1/logging";
import {SEFAction} from "@glass-project1/glass-toolkit/lib/providers";

const MoJBusinessRoutes: RouteDefinitions = {
    "handleResidencePermit": {
        path: "/sef",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class SEFBusinessService extends AbsBusinessService{

    constructor(server: any) {
        super(server, MoJBusinessRoutes)
    }

    private handleResidencePermit(data: {did: string}, req: any, callback: Callback){
        const self: SEFBusinessService = this;
        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);

        const evidenceRequest = new GlassEvidenceRequest({
            requestId: Date.now().toString(),
            purpose: this.locale.get("purposes.issue.residence") as string,
            action: SEFAction.ISSUE_RESIDENCE_PERMIT_PERMANENT,
            requesterDID: self.getDID().getIdentifier(),
            receiverDID: did,
            payload: ["egov.*.moj.id.*", `*.${self.getDID().data?.identifier?.split(".")[1]}.*.contract.work.*`]
        })

        self.handleRequest(evidenceRequest)
            .then(result => {
                callback(undefined, {did: did});
            })
            .catch(e => errorCallback.call(self, e, callback))
    }
}