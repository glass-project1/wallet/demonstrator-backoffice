import {GlassEvidenceRequest, HTTP_METHODS} from "@glass-project1/glass-toolkit";
import {RouteDefinitions} from "../utils";
import {AbsBusinessService} from "../general/AbsBusinessService";
import {Callback, errorCallback} from "@glass-project1/logging";
import {MoJActions} from "@glass-project1/glass-toolkit/lib/providers";

const MoJBusinessRoutes: RouteDefinitions = {
    "handleSocialSecurity": {
        path: "/social",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class BusinessService extends AbsBusinessService{

    constructor(server: any) {
        super(server, MoJBusinessRoutes)
    }

    private handleSocialSecurity(data: {did: string}, req: any, callback: Callback){
        const self: BusinessService = this;
        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);

        const evidenceRequest = new GlassEvidenceRequest({
            requestId: Date.now().toString(),
            purpose: this.locale.get("purposes.issue.social") as string,
            action: MoJActions.ISSUE_SS_CARD,
            requesterDID: self.getDID().getIdentifier(),
            receiverDID: did,
            payload: ["egov.*.moj.birth.*", "egov.*.moj.id.*"]  
        })

        self.handleRequest(evidenceRequest)
            .then(result => {
                callback(undefined, {did: did});
            })
            .catch(e => errorCallback.call(self, e, callback))
    }
}