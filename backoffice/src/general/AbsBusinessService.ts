import {GlassDID, GlassRequest, GlassWalletManager, HTTP_METHODS, recursiveQueryValidation} from "@glass-project1/glass-toolkit";
import {inject} from "@glass-project1/db-decorators";
import {LocaleService} from "@glass-project1/localization";
import {RouteDefinitions} from "@glass-project1/glass-toolkit/lib/providers/types";
import {ApiHubMiddleware} from "@glass-project1/glass-toolkit/lib/providers/ApiHubMiddleware";
import {isReady} from '../readyness';
import { Callback, Err, errorCallback } from "@glass-project1/logging";
import { parsePathParamsToKey, parseQueryParamsToDsuQuery, parseRequestBody } from "@glass-project1/glass-toolkit/lib/providers/services/http";

export abstract class AbsBusinessService extends ApiHubMiddleware {

    @inject("GlassWalletManager")
    protected readonly walletManager!: GlassWalletManager;

    @inject("GlassLocaleService")
    protected readonly locale!: LocaleService;

    protected constructor(server: any, routes: RouteDefinitions) {
        super(server, routes);
    }

    protected isReady(): boolean {
        return isReady();
    }

    protected handlerRequest(methodName: string, req: any, method: HTTP_METHODS, callback: Callback): void{
        const self: {[indexer: string]: any} = this;
        const {...pathParams} = req.params;

        if (!(methodName in self))
            return errorCallback.call(self, "No such method: {0}", callback, methodName);

        const methods: any = {
            GET: () => {
                const key = parsePathParamsToKey(pathParams);
                self[methodName].call(self, key, req, callback);
            },
            GET_ALL: () => {
                const {query, sort, limit} = parseQueryParamsToDsuQuery(pathParams);
                self[methodName].call(self, query, sort, limit, req, callback);
            },
            POST: () => {
                parseRequestBody(req, (err, body: any) => {
                    if (err)
                        return callback(err);

                    self.walletManager.resolveDID(body.did, (err: Err, did: GlassDID) => {
                        if(err || !did)
                            return callback(err || `Failed to resolve did ${body.did}`);
                         
                        if(typeof did.data?.proxy !== "undefined" && !recursiveQueryValidation(self.getDID().data?.identifier, did.data?.proxy?.permissions))
                            return callback(`This Proxy DID doens't have permission to comunicate with this namespace`);

                        self[methodName].call(self, body, req, callback);
                    })
                })
            },
            PUT: () => {
                parseRequestBody(req, (err, body: any) => {
                    if (err)
                        return callback(err);
                    const key = parsePathParamsToKey(pathParams);
                    self[methodName].call(self, key, body, req, callback);
                })
            },
            DELETE: () => {
                const key = parsePathParamsToKey(pathParams);
                self[methodName].call(self, key, req, callback);
            }
        }

        methods[method]();
    }

    protected async handleRequest(request: GlassRequest<any>){
        const self: AbsBusinessService = this;
        return new Promise((resolve, reject) => {
            self.walletManager.handleRequest(request)
                .then(resolve)
                .catch(reject)
        })
    }

    protected getDID(): GlassDID{
        return this.walletManager.did as unknown as GlassDID;
    }
}