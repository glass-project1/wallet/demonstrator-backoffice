import {GlassEvidence, HTTP_METHODS} from "@glass-project1/glass-toolkit";
import {
    ApiHubDsuService,
    ApiHubServiceRoute,
} from "@glass-project1/glass-toolkit/lib/providers";
import {repoGetter} from "../utils";

const routes: ApiHubServiceRoute[] = [
    {method: HTTP_METHODS.GET, pathParams: ["id"]},
    {method: HTTP_METHODS.GET_ALL}
]

export class EvidenceService extends ApiHubDsuService<GlassEvidence>{

    constructor(server: any) {
        super(server, "/evidence", routes, GlassEvidence, repoGetter("WalletEvidenceManager"));
    }
}