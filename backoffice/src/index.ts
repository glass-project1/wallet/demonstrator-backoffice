import "@glass-project1/logging";
import "@glass-project1/opendsu-types";
import "@glass-project1/decorator-validation";
import "@glass-project1/db-decorators";
import "@glass-project1/ui-decorators";
import "@glass-project1/dsu-blueprint";
import "@glass-project1/localization";
import "@glass-project1/glass-toolkit";
import {
    BootFunction,
    getMiddlewareEnvironment,
    getServiceWalletSSI,
    InitFunction,
    loadServiceWalletFromSSI,
    MiddlewareEnvironmentDefinition,
    SupportedMiddlewares
} from "./utils";
import {Callback, CriticalError, debug, Err, error, errorCallback, getLogger, info} from "@glass-project1/logging";
import {boot as MojBoot, init as MojInit} from './moj';
import {boot as BankBoot, init as BankInit} from './bank';
import {boot as PDMFCBoot, init as PDMFCInit} from './pdmfc';
import {boot as LandlordBoot, init as LandlordInit} from './landlord'
import {boot as SEFBoot, init as SEFInit} from './sef'
import {boot as DisabilityReportBoot, init as DisabilityReportInit} from './disability'
import {init as trustInit} from "@glass-project1/glass-toolkit/lib/providers/trust/index"
import {EGovServiceWallet, GlassDID, GlassDIDSchema, registerGlassDIDMethod} from "@glass-project1/glass-toolkit";
import {KeySSI} from "@glass-project1/opendsu-types";
import {BackendFileServiceImp} from "@glass-project1/glass-toolkit/lib/providers/BackendFileService";
import {getServiceFromPersistence, setSSIInPersistence} from "@glass-project1/glass-toolkit/lib/providers/persistence";
import {setReady} from "./readyness";


export const init = async (server: any, environment?: MiddlewareEnvironmentDefinition) => {
    registerGlassDIDMethod();
    environment = environment && typeof environment !== 'function' ? environment : getMiddlewareEnvironment();

    getLogger().setLevel(environment.logging);
    debug.call(getMiddlewareEnvironment, "Running Glass Middleware with environment:\n{0}", JSON.stringify(environment, undefined, 2))
    const callbackGenerator = function(middleware: SupportedMiddlewares){

        const cb: Callback = function(err?: Err){
            if (err){
                return error.call(middleware, err as any)
                // const timeout =  (environment as MiddlewareEnvironmentDefinition).timeout;
                // info.call(middleware, "Rebooting middleware {0} in {1} s...", (environment as MiddlewareEnvironmentDefinition).name, timeout)
                // setTimeout(() => {
                //     init(server, environment);
                // }, timeout)
            }
            info.call(init, "MiddleWare {0} initialized", (environment as MiddlewareEnvironmentDefinition).name);
        }

        return cb;
    }

    const callback: Callback = callbackGenerator(environment.name);

    let initMethod: InitFunction;
    let bootMethod: BootFunction;

    switch(environment.name){
        case SupportedMiddlewares.MoJ:
            initMethod = MojInit;
            bootMethod = MojBoot;
            break;
        case SupportedMiddlewares.BANK:
            initMethod = BankInit
            bootMethod = BankBoot
            break;
        case SupportedMiddlewares.PDMFC:
            initMethod = PDMFCInit;
            bootMethod = PDMFCBoot;
            break;
        case SupportedMiddlewares.LANDLORD:
            initMethod = LandlordInit;
            bootMethod = LandlordBoot;
            break;
        case SupportedMiddlewares.SEF:
            initMethod = SEFInit;
            bootMethod = SEFBoot;
            break;
        case SupportedMiddlewares.DISABILITY:
            initMethod = DisabilityReportInit
            bootMethod = DisabilityReportBoot
            break;
        case SupportedMiddlewares.TRUST:
            return trustInit(server, environment, {
                workerLimit: 10,
                fileName: "./../node_modules/@glass-project1/glass-toolkit/lib/providers/concurrency/worker.js",
                taskPath: "./../node_modules/@glass-project1/glass-toolkit/lib/providers/concurrency/tasks",
                openDSUPath: "./../privatesky/psknode/bundles/openDSU.js"
            });
        case SupportedMiddlewares.UNKNOWN:
        default:
            return error.call(init, "Invalid Middleware: {0}", environment.name);
    }


    initMethod(server, environment as MiddlewareEnvironmentDefinition, callback)

    const cb = function(err: Err){
        if (err)
            return callback(err);
        setReady()
        callback();
    }

    const fileService = new BackendFileServiceImp(`../backoffice/assets/${environment.name}`)

    const logSchema = function(msg: string, did: GlassDID){
        const schema: GlassDIDSchema = did.data as GlassDIDSchema;
        info.call(init, msg, schema.toString())
    }

    getServiceFromPersistence(environment, (err: Err, keySSI?: KeySSI) => {
        if (!err && keySSI)
            return loadServiceWalletFromSSI(keySSI, (err: Err, blueprint?: EGovServiceWallet) => {
                if (err || !blueprint)
                    throw err || new CriticalError("Missing blueprint", init);
                logSchema("Service Wallet loaded with DID Schema: {0}", blueprint.did as GlassDID);
                bootMethod(blueprint, fileService, false, cb);
            })

        getServiceWalletSSI(environment as MiddlewareEnvironmentDefinition, fileService, (err: Err, keySSI?: KeySSI) => {
            if (err || !keySSI)
                return errorCallback.call(init, err || "missing SSI from EGOV", callback);
            loadServiceWalletFromSSI(keySSI, (err: Err, blueprint?: EGovServiceWallet) => {
                if (err || !blueprint)
                    return errorCallback.call(init, err || "Failed to load blueprint from dsu", callback);
                setSSIInPersistence(environment as MiddlewareEnvironmentDefinition, keySSI.getIdentifier(), (err: Err) => {
                    if (err)
                        return errorCallback.call(init, err || "Failed to store Service Wallet SSI in enclave", callback);
                    logSchema("Service Wallet created with DID Schema: {0}", blueprint.did as GlassDID);
                    bootMethod(blueprint, fileService, false, cb)
                })
            })
        })
    })
}