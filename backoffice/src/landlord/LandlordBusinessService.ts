import { GlassEvidenceRequest, HTTP_METHODS } from "@glass-project1/glass-toolkit";
import { RentalActions } from "@glass-project1/glass-toolkit/lib/providers";
import { Callback, errorCallback } from "@glass-project1/logging";
import { IncomingMessage } from "http";
import { AbsBusinessService } from "../general/AbsBusinessService";
import { RouteDefinitions } from "../utils";


const LandlordBusinessRoutes: RouteDefinitions = {
    "handleRentalContract" : {
        path: "/landlord",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class LandlordBusinessService extends AbsBusinessService{
    constructor(server: any){
        super(server, LandlordBusinessRoutes)
    }

    protected handleRentalContract(data: {did: string}, req: IncomingMessage, callback: Callback){
        const self: LandlordBusinessService = this;

        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);
   
    const evidenceRequest = new GlassEvidenceRequest({
        requestId: Date.now().toString(),
        purpose: this.locale.get("purposes.issue.rental") as string,
        action:  RentalActions.ISSUE_RENTAL_CONTRACT,
        requesterDID: self.getDID().getIdentifier(),
        receiverDID: did,
        payload: ["egov.*.moj.id.*", "*.*.*.contract.work.*"]
    })

    self.walletManager.handleRequest(evidenceRequest)
        .then((result: any) =>{callback(undefined, result)})
        .catch((e: any) => {errorCallback.call(self, e, callback)})

    }
}