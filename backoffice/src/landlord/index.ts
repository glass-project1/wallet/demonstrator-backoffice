import {BootFunction, InitFunction, MiddlewareEnvironmentDefinition} from "../utils";
import {Callback, Err, errorCallback} from "@glass-project1/logging";
import { ApiHubService, apiHubServiceInitializer, bootBankWallet, bootLandlordWallet } from "@glass-project1/glass-toolkit/lib/providers";
import { EvidenceService } from "../general/EvidenceService";
import { EGovServiceWallet, MessagingHub } from "@glass-project1/glass-toolkit";
import { getInjectablesRegistry } from "@glass-project1/db-decorators";
import {BackendFileService} from "@glass-project1/glass-toolkit/lib/providers/BackendFileService";
import { LandlordBusinessService } from "./LandlordBusinessService";

export const init: InitFunction = (server: any, environment: MiddlewareEnvironmentDefinition, callback: Callback) => {
    try {
        apiHubServiceInitializer(server, [
            EvidenceService,
            LandlordBusinessService as unknown as ApiHubService
        ])
    } catch (e: any){
        errorCallback("Middleware failed: {0}", callback, e.message || e)
    }
}

export const boot: BootFunction = function(blueprint: EGovServiceWallet, fileService: BackendFileService, init: boolean, callback: Callback){
    bootLandlordWallet(blueprint, fileService, init, (err: Err) => {
        if(err)
            return errorCallback.call(boot, err || "Missing manager", callback);

        const hub: MessagingHub = getInjectablesRegistry().get("MessageHub") as MessagingHub;
        if(!hub)
            return errorCallback.call(boot, err || "Missing Message hub", callback);   

        (hub as unknown as {startListening: () => void}).startListening();
        callback();
    });
}