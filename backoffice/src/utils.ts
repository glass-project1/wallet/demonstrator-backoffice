import {Callback, debug, Err, errorCallback, LoggedError, LOGGER_LEVELS, stringFormat} from "@glass-project1/logging";
import {GenericCallback, getHttpApi, getKeySSIApi, KeySSI} from "@glass-project1/opendsu-types";
import {generateServiceEnvironment, getEnvironmentFromProcess, BackendFileService,
    BackendFileServiceImp} from "@glass-project1/glass-toolkit/lib/providers";
import {
    AbsDSUTableManager,
    EGovServiceWallet,
    GlassDIDSchema,
    HTTP_METHODS,
    ServiceEnvironmentDefinition
} from "@glass-project1/glass-toolkit";
import {DBModel} from "@glass-project1/db-decorators/lib/model/DBModel";
import {getInjectablesRegistry} from "@glass-project1/db-decorators";
import {OpenDSURepository} from "@glass-project1/dsu-blueprint";
import {getServiceFromPersistence} from "@glass-project1/glass-toolkit/lib/providers/persistence";
import {RouteDefinition} from "@glass-project1/glass-toolkit/lib/providers/types";
export type RouteDefinitions = {
    [indexer: string]: RouteDefinition
}

export type ApiHubMiddlewareInitFunction = (server: any, config: {}, callback?: Callback) => void;
export type InitFunction = (service: any, environment: MiddlewareEnvironmentDefinition, callback: Callback) => void;
export type BootFunction = (blueprint: EGovServiceWallet, fileService: BackendFileService, init: boolean, callback: Callback) => void;

export const ProcessMiddlewareKey = "GlassMiddleware"
export const ProcessMiddlewareTimeoutKey = "GlassMiddlewareTimeout"
export const ProcessMiddlewareLoggingKey = "GlassMiddlewareLogging"

export enum SupportedMiddlewares {
    MoJ = "moj",
    BANK = "bank",
    PDMFC = "pdmfc",
    LANDLORD= "landlord",
    DISABILITY = "disability",
    SEF="sef",
    TRUST = "trust",
    UNKNOWN = "unknown"
}
export type MiddlewareEnvironmentDefinition = ServiceEnvironmentDefinition & {
    name: SupportedMiddlewares,
    logging: number,
    timeout: number
}

export enum EndPoint {
    GLASS = "glass.json",
    EGOV = "egov.json",
    EGOV_ID = "egovid.json"
}

export type EndpointDef = {
    "baseEndpoint": string,
    "baseEndpointSuffix": string,
    domain?: string
}

export function repoGetter<T extends DBModel>(repoName: string){
    return getInjectablesRegistry().get(repoName) as unknown as AbsDSUTableManager<T>;
}

export function getMiddlewareEnvironment(): MiddlewareEnvironmentDefinition{
    const middleware: SupportedMiddlewares = process.env[ProcessMiddlewareKey] as SupportedMiddlewares || SupportedMiddlewares.UNKNOWN;
    const middlewareTimeout: number = process.env[ProcessMiddlewareTimeoutKey] ? parseInt(process.env[ProcessMiddlewareTimeoutKey]) || 1000 : 1000;
    const middlewareLogging: number = process.env[ProcessMiddlewareLoggingKey] ? parseInt(process.env[ProcessMiddlewareLoggingKey]) || LOGGER_LEVELS.DEBUG : LOGGER_LEVELS.DEBUG;
    const serviceEnvironment = generateServiceEnvironment(getEnvironmentFromProcess());
    return Object.assign({name: middleware, timeout: middlewareTimeout, logging: middlewareLogging}, serviceEnvironment) as MiddlewareEnvironmentDefinition;
}

export async function getEndpoints(endpoint: EndPoint): Promise<EndpointDef>{
    const fs = require('fs');
    const path = require('path');

    let endpoints: EndpointDef;
    try {
        endpoints = JSON.parse(fs.readFileSync(path.join(process.cwd(), "../apihub-root/demonstrator/assets/endpoints", endpoint))) as EndpointDef
    } catch (e: any) {
        throw new Error(`Failed to read ${endpoint} over: ${e.message || e}`);
    }
    return endpoints;
}

export function getHeaders(httpMethod: HTTP_METHODS, body?: {}){
    if (!body && httpMethod !== HTTP_METHODS.GET)
        throw new LoggedError("No body provided to request")

    const headers: {[indexer: string]: any} = {};
    headers['Content-Type'] = 'application/json';

    if (httpMethod !== HTTP_METHODS.GET)
        headers['Content-Length'] = `${Buffer.byteLength(JSON.stringify(body))}`;
    return headers;
}

export function getServiceWalletSSI(environment: MiddlewareEnvironmentDefinition, fileService: BackendFileServiceImp, callback: GenericCallback<KeySSI>){
    getServiceFromPersistence(environment, (err: Err, serviceSSI?: KeySSI) => {
        if (!err && serviceSSI)
            return callback(undefined, serviceSSI);
        if (!err)
            return errorCallback.call(getServiceWalletSSI, "No service ssi received", callback);
        getEndpoints(EndPoint.EGOV)
            .then(endpoint => {
                const countryCode = process.env["GLASS_COUNTRY_CODE"];
                endpoint.baseEndpoint = stringFormat(endpoint.baseEndpoint, countryCode as string);
                
                debug.call(getServiceWalletSSI, "Retrieved EGov endpoint to call for ssi: {0}", JSON.stringify(endpoint, undefined, 2))
                fileService.getSchema((err: Err, schema?: {}) => {
                    if (err || !schema)
                        return errorCallback(err || "missing schema", callback);
                    const didSchema = new GlassDIDSchema(schema);
                    const errs = didSchema.hasErrors("createdOn", "createdBy", "updatedOn", "updatedBy");
                    if (errs)
                        return errorCallback("Schema from file is invalid: {0}", callback, errs.toString());

                    debug.call(getServiceWalletSSI, "Requesting DID schema: {0}", didSchema.toString())
                    const data = {
                        didSchema: schema,
                        // domain: environment.vaultDomain
                        domain: `egov.${countryCode?.toLowerCase()}`
                    }
                    const httpMethod = HTTP_METHODS.POST;
                    const headers = getHeaders(httpMethod, data)
                    const reqOptions = {
                        headers: headers,
                        method: HTTP_METHODS.POST,
                        mode: "cors",
                        body: JSON.stringify(data)
                    }

                    const url = `${endpoint.baseEndpoint}${endpoint.baseEndpointSuffix}/egov/setup/createNewServiceWallet`
                    debug.call(getServiceWalletSSI, "Calling EGOV on {0} to create new service wallet with options {1}.", url, JSON.stringify(reqOptions, undefined, 2))
                    getHttpApi().fetch(url, reqOptions)
                        .then(async (response: any) => {
                            let ssi: KeySSI;
                            try {
                                response = await response.text();
                                debug.call(getServiceWalletSSI, "received {0}", response);
                                ssi = getKeySSIApi().parse(response);
                            } catch (e: any){
                                return errorCallback.call(getServiceWalletSSI, e, callback);
                            }
                            callback(undefined, ssi);
                        })
                        .catch((e: any) => errorCallback.call(getServiceWalletSSI, e, callback))
                })
            })
            .catch(e => errorCallback.call(getServiceWalletSSI, e, callback))
    })
}

export function loadServiceWalletFromSSI(ssi: string | KeySSI, callback: Callback){
    const repo = new OpenDSURepository(EGovServiceWallet);
    repo.read(ssi, (err: Err, model?: EGovServiceWallet) => {
        if (err || !model)
            return errorCallback.call(loadServiceWalletFromSSI, err || "Missing model", callback);
        callback(undefined, model);
    })
}

export function getFileService(environment: MiddlewareEnvironmentDefinition, path: string = "./assets/{0}"): BackendFileService {
    path = stringFormat(path, environment.name);
    return new BackendFileServiceImp(path);
}

