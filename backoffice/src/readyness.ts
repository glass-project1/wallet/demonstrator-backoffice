let ready = false;

export function isReady(){
    return ready;
}

export function setReady(r = true){
    ready = r;
}