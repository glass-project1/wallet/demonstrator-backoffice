
import { GlassEvidenceRequest, HTTP_METHODS } from "@glass-project1/glass-toolkit";
import { DisabilityReportActions } from "@glass-project1/glass-toolkit/lib/providers";
import { Callback, errorCallback } from "@glass-project1/logging";
import { IncomingMessage } from "http";
import { AbsBusinessService } from "../general/AbsBusinessService";
import { RouteDefinitions } from "../utils";


const DisabilityBusinessRoutes: RouteDefinitions = {
    "handleDisabilityReport" : {
        path: "/disability",
        method: HTTP_METHODS.POST,
        pathParams: ["did"]
    }
}

export class DisabilityBusinessService extends AbsBusinessService {
    constructor(server: any) {
        super(server, DisabilityBusinessRoutes)
    }

    protected handleDisabilityReport(data: {did: string}, req: IncomingMessage, callback: Callback){
        const self : DisabilityBusinessService = this;

        const {did} = data;

        if(!did)
            return errorCallback.call(self, `No DID Received on data: ${data}`, callback);
       
        const evidenceRequest = new GlassEvidenceRequest({
            requestId: Date.now().toString(),
            purpose: this.locale.get("purposes.issue.disability") as string,
            action: DisabilityReportActions.ISSUE_DISABILITY_REPORT,
            requesterDID: self.getDID().getIdentifier(),
            receiverDID: did,
            payload: ["egov.*.moj.id.*", "egov.*.ibs.passport.*"]
        })

        self.walletManager.handleRequest(evidenceRequest)
            .then((result: any) =>{callback(undefined, result)})
            .catch((e: any) => {errorCallback.call(self, e, callback)})
    }
}