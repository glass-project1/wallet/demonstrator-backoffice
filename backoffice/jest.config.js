
module.exports = {
  verbose: true,
  transform: {'^.+\\.ts?$': 'ts-jest'},
  testRegex: '/tests/.*\\.(test|spec)\\.(ts|tsx)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  // globalSetup: "./node_modules/@glass-project1/dsu-utils/src/jest-setup.js",
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{ts,jsx}'],
  coverageDirectory: "./workdocs/coverage",
  coverageReporters: [
    "json-summary",
    "text-summary",
    "text",
    "html"
  ],
  reporters: [
    "default",
    ["jest-junit", {outputDirectory: './workdocs/coverage', outputName: "junit-report.xml"}]
  ]
};