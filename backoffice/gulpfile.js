const path = require('path');
const gulp = require('gulp');
const {src, dest, parallel, series} = gulp;
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if')
const merge = require('merge-stream');
const named = require('vinyl-named');
const webpack = require('webpack-stream');
const {argParser} = require('@glass-project1/dsu-utils/src/utils');
const rename = require('gulp-rename');

const glassPrefix = "@glass-project1/"
let {name, version} = require('./package.json');
const externalsRegistry = [];

if (name.indexOf(glassPrefix) !== -1)
    name = name.substr(glassPrefix.length).replaceAll("-", "_");

const STAGES = {
    BUILD: "build",
    DEPLOY: "deploy"
}

const MODES = {
    DEVELOPMENT: "development",
    PRODUCTION: "production"
}

const defaultOptions = {
    mode: MODES.DEVELOPMENT,
    stage: STAGES.BUILD,
    name: name
}

const config = argParser(defaultOptions, process.argv)

function isDev(){
    return config.mode === MODES.DEVELOPMENT;
}

function isProd(){
    return config.mode === MODES.PRODUCTION;
}

function isBuild(){
    return config.stage === STAGES.BUILD;
}

function isDeploy(){
    return config.stage === STAGES.DEPLOY;
}

function getWebpackConfig(isESM){
    const webPackConfig =  {
        mode: config.mode, // can be changed to production to produce minified bundle

        externals:[
            {
            },
            function ({ context, request }, callback) {
                if (/^(.*?(\bglass-project1\b)[^$]*)$/.test(request)) {
                    let splitRequest = request.split("/");
                    let name = splitRequest[1];
                    externalsRegistry.push(request);
                  return callback(null, `./${name}${isESM ? ".esm" : ""}.js`);
                }
                callback();
            },
        ],

        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: [{
                        loader: 'ts-loader',
                        options: {
                            configFile: 'tsconfig.json'
                        }
                    }],
                    include:[
                        path.resolve(__dirname, "src")
                    ],
                    exclude: /node_modules/,
                }
            ]
        },

        resolve: {
            extensions: ['.ts', '.js'],
            fallback: {
                path: false,
                fs: false,
                util: false
            }
        },

        output: {
            filename: `${config.name}.bundle.${isProd() ? 'min.' : ''}${isESM ? 'esm.' : ''}js`,
            path: path.resolve(__dirname, "dist/"),
            library: {
                type: "module",
            },
        }
    }

  
    if(isESM)
        webPackConfig.experiments = {outputModule: true} 
    else
        webPackConfig.output = Object.assign({}, webPackConfig.output, {globalObject: 'this', library: config.name, libraryTarget: "umd", umdNamedDefine: true,});
    
    if (isDev())
        webPackConfig.devtool = 'eval-source-map';

    return webPackConfig;
}

function exportDefault(){
    const tsProject = ts.createProject('tsconfig.json', {
        target: "es6",
        module: "commonjs",
        declaration: true,
        declarationMap: true,
        emitDeclarationOnly: false,
        isolatedModules: false
    });

    const stream =  src('./src/**/*.ts')
        .pipe(gulpIf(isDev(), sourcemaps.init()))
        .pipe(tsProject())

    return merge([
        stream.dts.pipe(dest("lib")),
        stream.js.pipe(gulpIf(isProd(), uglify())).pipe(gulpIf(isDev(), sourcemaps.write())).pipe(dest("lib"))
    ])
}
//
// function exportBundles(isEsm){
//     const entryFile = "src/index.ts"
//     return src(entryFile)
//         .pipe(named())
//         .pipe(webpack(getWebpackConfig(isEsm)))
//         .pipe(dest(`./dist${isEsm ? '/esm' : ""}`));
// }
//
// function exportESMDist(){
//     return exportBundles(true);
// }
//
// function exportJSDist(){
//     return exportBundles(false);
// }
//
// function testPrep(){
//     let operations = [];
//
//     const source = "./";
//     const node_modules = "node_modules/"
//     const glass = "@glass-project1/"
//     const dist = "dist/";
//     const esm = "esm";
//     const bundle = ".bundle"
//     const extension = ".js"
//     const destination = "./tests/web/bundles/"
//
//
//     const createStream = function(isExternal, isESM, name){
//         return src(`${source}${isExternal ? `${node_modules}${glass}${name}/`: ""}${dist}${isESM ? esm + "/" : ""}${name.replace("-", "_")}${bundle}${isESM ? "." + esm : ""}${extension}`)
//         .pipe(rename(`${name}${isESM ? "." + esm : ""}${extension}`))
//         .pipe(dest(destination))
//     }
//
//     externalsRegistry.forEach(key => {
//         if(key.includes("@glass-project1")){
//             let keySplit = key.split("/");
//             operations.push(createStream(true, true, keySplit[1]));
//             operations.push(createStream(true, false, keySplit[1] ));
//         }
//     })
//
//     operations.push(createStream(false, true, config.name.replace("_", "-")));
//     operations.push(createStream(false, false, config.name.replace("_", "-")))
//
//
//     return merge(operations);
// }

function controlFlow(){
    if (isDeploy())
        return series(parallel(exportDefault));
    if (isBuild())
        return series(exportDefault);
}

exports.default = controlFlow()