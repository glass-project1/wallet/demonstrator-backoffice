import { DBModel } from "@glass-project1/db-decorators";
import { constructFromBlueprint } from "@glass-project1/dsu-blueprint";

export class EGovEntity extends DBModel {
    did?: string = undefined;
    eGovIdId?: string = undefined;
    walletKeySSI?: string = undefined;
    createdOn?: string = undefined;


    constructor(blueprint: EGovEntity | {}){
        super(blueprint);
        constructFromBlueprint<EGovEntity>(this, blueprint)
    }
}