import {DemonstratorInfo} from "./demonstrators";

export const MOJ_OPTIONS: DemonstratorInfo[] = [
    {
        id: "social",
        icon: "person-circle",
        route: "/egov-moj/social",
    },
    {
        id: "sef",
        icon: "home",
        route: "/sef/permit",
    },
    {
        id: "seftemp",
        icon: "pricetags",
        route: "/egov-moj/seftemp",
    }
]
