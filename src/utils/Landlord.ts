import { injectable } from "@glass-project1/db-decorators";
import { NestJSRestDatabase, RestConfig } from "@glass-project1/glass-toolkit";
import { Callback } from "@glass-project1/logging";
import { DemonstratorInfo } from "./demonstrators";

export const LANDLORD_OPTIONS: DemonstratorInfo[] = [
    {
        id: "issue-rental",
        icon: "contract",
        route: "/landlord/issue-contract"
    }
]

@injectable("LandlordBusinessManager")
export class  LandlordBusinessManager {
    readonly config: RestConfig

    constructor(config?: RestConfig) {
        this.config = config || {
            baseEndpoint: "http://localhost:8081", 
            baseEndpointSuffix: undefined,
            authorizationType: undefined,
            authorizationToken: undefined,
            mode: "cors",
            pathParamsSeparator: "|",
            domain: undefined
        }

    }

    requestRentalContractEvidences(did: string, callback: Callback) {
        new NestJSRestDatabase(this.config).insertRecord("landlord", did, {did: did}, callback)
    }
}

