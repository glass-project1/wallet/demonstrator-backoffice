import {NestJSRestDatabase, RestConfig, RestDatabase} from "@glass-project1/glass-toolkit";
import {Callback} from "@glass-project1/logging";
import {injectable} from "@glass-project1/db-decorators";

@injectable("MoJBusinessManager")
export class MoJBusinessManager {

    readonly config: RestConfig

    constructor(config?: RestConfig) {
        this.config = config || {
            baseEndpoint: "http://localhost:3001",  //"https://egov-id-{0}-dev.glass.pdmfc.com",
            baseEndpointSuffix: "/borest/egovid",
            authorizationType: undefined,
            authorizationToken: undefined,
            mode: "cors",
            pathParamsSeparator: "|",
            domain: undefined
        };
    }

    requestSocialSecurityCardEvidences(did: string, country: string = "pt", callback: Callback){
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
        
        new NestJSRestDatabase(config).insertRecord("egovsocial", did, {did: did}, callback)
    }
}