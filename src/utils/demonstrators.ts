export const DEMONSTRATORS_LIST: DemonstratorInfo[] = [
    {
        id: "egov",
        icon: "person-circle",
        route: "countries/egov",
    },
    {
        id: "egov-moj",
        icon: "id-card",
        route: "countries/egov-moj",
    },
    {
        id: "bank",
        icon: "card",
        route: "/bank",
    },
    {
        id: "pdmfc",
        icon: "hammer-outline",
        route: "/pdmfc"
    },
    {
        id: "landlord",
        icon: "home",
        route: "/landlord"
    },
    {
      id: "disability",
      icon: "medkit-outline",
      route: "countries/egov-moh",
    },
    {
      id: "sign-on",
      icon: "key",
      route: "/signon",
    },
    {
        id: "konstantinos",
        icon: "invert-mode",
        route: "konstantinos",
    },
]


export type DemonstratorInfo = {
    /**
     * Defines the app id
     */
    id: string
    /**
     * Defines the app icon
     */
    icon: string
    /**
     * Defines the app route
     */
    route: string
}

