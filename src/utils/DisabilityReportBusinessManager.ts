import { injectable } from "@glass-project1/db-decorators";
import { NestJSRestDatabase, RestConfig } from "@glass-project1/glass-toolkit";
import { Callback } from "@glass-project1/logging";
import { DemonstratorInfo } from "./demonstrators";

export const DISABILITY_REPORT_OPTIONS: DemonstratorInfo[] = [
    {
        id: "issue-report",
        icon: "medkit-outline",
        route: "/disability/issue-report",
    }
]


@injectable("DisabilityReportBusinessManager")
export class DisabilityReportBusinessManager {
    readonly config: RestConfig

    constructor(config?: RestConfig) {
    
        this.config = config || {            
            baseEndpoint: "http://localhost:3004",  
            baseEndpointSuffix: "/borest/egovhealth",
            authorizationType: undefined,
            authorizationToken: undefined,
            mode: "cors",
            pathParamsSeparator: "|",
            domain: undefined
        }

    }

    requestDisabilityReportEvidences(did: string, country: string = "pt", callback: Callback) {
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
        new NestJSRestDatabase(config).insertRecord("disabilityreport", did, {did: did}, callback)
    }
    requestDisabilityReportConvertEvidences(did: string, country: string = "pt", callback: Callback) {
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
        new NestJSRestDatabase(config).insertRecord("disabilityreportconvert", did, {did: did}, callback)
    }
    requestHealthInsuranceEvidences(did: string, country: string = "pt", callback: Callback) {
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
        new NestJSRestDatabase(config).insertRecord("healthinsurance", did, {did: did}, callback)
    }
}
