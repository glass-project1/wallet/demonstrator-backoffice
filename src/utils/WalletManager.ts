import { DBandDIDHolder, TableManager} from "@glass-project1/glass-toolkit";
import {DBModel, injectable} from "@glass-project1/db-decorators";
import { constructFromBlueprint } from "@glass-project1/dsu-blueprint";

/**
 * A Simple wallet manager that doens't require inputs.
 *
 * However, since that class is extended it cannot be an injectable,
 * and another way would have to be found to pass the object between components.
 *
 * By extending the class and marking as {@link injectable} we can easily
 * get it on whatever webcomponents need it via {@link inject}
 *
 * @class EGovWalletManager
 * @extends DBModel
 *
 * @see injectable under category "WalletManager"
 */

@injectable("WalletManager")
export class EGovWalletManager extends DBModel {
    private readonly managerCache: {[indexer: string]: TableManager<any>} = {}

    constructor(manager: DBandDIDHolder | {}){
        super();
        constructFromBlueprint(this, manager);
    }
}