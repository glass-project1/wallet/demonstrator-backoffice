import {RestAbsTableManager} from "./RestAbsTableManager";
import {GlassEvidenceRequest, DBandDIDHolder, RestConfig} from "@glass-project1/glass-toolkit";

const DEFAULT_CONFIG_EGOVID: RestConfig = {
    baseEndpoint: "http://localhost:3001",  //"https://egov-id-pt-dev.glass.pdmfc.com",
    baseEndpointSuffix: "/borest/egovid",
    authorizationType: undefined,
    authorizationToken: undefined,
    mode: "cors",
    pathParamsSeparator: "|",
    domain: undefined
}

export class MoJEvidenceRequestManager extends RestAbsTableManager<GlassEvidenceRequest>{
    constructor(walletManager: DBandDIDHolder, config?: RestConfig){
        super(walletManager, GlassEvidenceRequest, "egovid", Object.assign({}, DEFAULT_CONFIG_EGOVID, config || {}));
    }
}