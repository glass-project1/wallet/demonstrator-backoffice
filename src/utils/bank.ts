import {DemonstratorInfo} from "./demonstrators";
import { DBModel, injectable} from "@glass-project1/db-decorators";
import { DBandDIDHolder, RestConfig } from "@glass-project1/glass-toolkit";
import { RestAbsTableManager } from "./RestAbsTableManager";
import { constructFromBlueprint } from "@glass-project1/dsu-blueprint";
import { GenericCallback } from "@glass-project1/opendsu-types";
import { CriticalError } from "@glass-project1/logging";

export const BANK_OPTIONS: DemonstratorInfo[] = [
    {
        id: "open-account",
        icon: "card",
        route: "/bank/open",
    }
]

const DEFAULT_CONFIG_BANK: RestConfig = {
    baseEndpoint: "http://localhost:8081", 
    baseEndpointSuffix: undefined,
    authorizationType: undefined,
    authorizationToken: undefined,
    mode: "cors",
    pathParamsSeparator: "|",
    domain: undefined
}

export class DIDEntity extends DBModel {
    did?: string = undefined;

    constructor(blueprint: DIDEntity | {}){
        super(blueprint);
        constructFromBlueprint<DIDEntity>(this, blueprint)
    }
}

@injectable("BankRestManager")
export class BankRestManager extends RestAbsTableManager<DIDEntity>{
    constructor(walletManager: DBandDIDHolder & DBModel, config: RestConfig){
        super(walletManager, DIDEntity, "bank", Object.assign({}, DEFAULT_CONFIG_BANK, config || {}))
        super.instantiateModel = this.instantiateModel
    }

    instantiateModel(record: any | DIDEntity, ...args: (any | GenericCallback<any>)[]): void {
        const callback: GenericCallback<any> = args.pop();
        if (!callback)
            throw new CriticalError("No callback", this);
        if(record.error)
            return callback(record);
        if (this.clazz)
            return callback(undefined, new this.clazz({did: record.senderDID}) as DIDEntity);
        callback(undefined, record as DIDEntity);
    }
}

