import { GlassModal, getGlassModal } from "@glass-project1/base-web-components";
import { LoggedError } from "@glass-project1/logging";


export interface ICountry {
  /**
   * Name 
   */
  name: "Portugal" | "Greece" | "Turkey" 
  /**
   * Alpha-2 Country code
   */
  code: "pt" | "gr" | "tk"
}

export function getCountry() {
  const countries: ICountry[]  = [{name: "Portugal", code: "pt"}, {name:"Greece",  code: "gr"}, {name:"Turkey",  code: "tk"}];

  let { pathname } = window.location;
  const countryName = pathname.split("/").pop();
  
  return countries.find((country: ICountry) => country.name.toLowerCase() == countryName);

}

export async function getProxyRequestDisplayModal(request: {data: string, label: string, header: string}, presentingElement?: HTMLElement){
    const modal: GlassModal = getGlassModal();
  
    if (!presentingElement)
      try {
        presentingElement = window.parent.document.querySelector("ion-router") as HTMLElement;
      } catch (e) {
        throw new LoggedError("Could not find presenting Element for the glass Modal")
      }
  
    if (!presentingElement)
      throw new LoggedError("Could not find presenting Element for the glass Modal")
    
    return modal.present("glass-proxy-request-display", presentingElement, {
      data: request.data as string, 
      header: request.header as string,
      label: request.label as string,
      modal: modal,
    })
}