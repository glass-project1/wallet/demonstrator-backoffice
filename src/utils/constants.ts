import { DemonstratorInfo } from "./demonstrators"

export const IMG_SOURCE = {
    PM: "assets/images/profile-male.svg",
    PF: "assets/images/profile-female.svg",
    MP: "assets/images/male-profile.svg",
    FP: "assets/images/female-profile.svg",
    M: "assets/images/male.svg",
    F: "assets/images/female.svg"
}

export enum RequestInfo {
    SOCIALPT = '{"evidences":"egov.*.moj.id.*,egov.*.moj.birth.*", "permissions":"egov.pt.moj,egov.pt.moh.social.*"}',
    SOCIALGR = '{"evidences":"egov.*.moj.id.*,egov.*.moj.birth.*", "permissions":"egov.gr.moj,egov.gr.moh.social.*"}',
    SOCIALTK = '{"evidences":"egov.*.moj.id.*,egov.*.moj.birth.*", "permissions":"egov.tk.moj,egov.tk.moh.social.*"}',

    SEFPT = '{"evidences":"egov.*.moj.id.*,*.pt.*.contract.work.*", "permissions":"egov.pt.ibs,egov.pt.ibs.permits.residence.*"}',
    SEFGR = '{"evidences":"egov.*.moj.id.*,*.gr.*.contract.work.*", "permissions":"egov.gr.ibs,egov.gr.ibs.permits.residence.*"}',
    SEFTK = '{"evidences":"egov.*.moj.id.*,*.tk.*.contract.work.*", "permissions":"egov.tk.ibs,egov.tk.ibs.permits.residence.*"}',

    SEFTEMPPT = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.pt.ibs,egov.pt.ibs.permits.residence.*"}',
    SEFTEMPGR = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.gr.ibs,egov.gr.ibs.permits.residence.*"}',
    SEFTEMPTK = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.tk.ibs,egov.tk.ibs.permits.residence.*"}',
    
    DISABILITYCONVERTPT = '{"evidences":"egov.*.moj.id.*,egov.*.moh.disability.*,egov.pt.moh.health.*,egov.pt.ibs.permits.residence.*", "permissions":"egov.pt.moh,egov.pt.moh.disability.*"}',
    DISABILITYCONVERTGR = '{"evidences":"egov.*.moj.id.*,egov.*.moh.disability.*,egov.gr.moh.health.*,egov.gr.ibs.permits.residence.*", "permissions":"egov.gr.moh,egov.gr.moh.disability.*"}',
    DISABILITYCONVERTTK = '{"evidences":"egov.*.moj.id.*,egov.*.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*", "permissions":"egov.tk.moh,egov.tk.moh.disability.*"}',

    DISABILITYPT = '{"evidences":"egov.*.moj.id.*,egov.*.ibs.passport.*", "permissions":"egov.pt.moh,egov.pt.moh.disability.*"}',
    DISABILITYGR = '{"evidences":"egov.*.moj.id.*,egov.*.ibs.passport.*", "permissions":"egov.gr.moh,egov.gr.moh.disability.*"}',
    DISABILITYTK = '{"evidences":"egov.*.moj.id.*,egov.*.ibs.passport.*", "permissions":"egov.tk.moh,egov.tk.moh.disability.*"}',

    INSURANCEPT = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.pt.moh,egov.pt.moh.health.*"}',
    INSURANCEGR = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.gr.moh,egov.gr.moh.health.*"}',
    INSURANCETK = '{"evidences":"egov.*.moj.id.*", "permissions":"egov.tk.moh,egov.tk.moh.health.*"}',

    BANKPT = '{"evidences":"egov.*.mof.tax.*,egov.*.moj.id.*","permissions":"finance.pt.some-portuguese-bank,finance.pt.some-portuguese-bank.accounts.proof.*"}',
    WORKPDMFC = '{"evidences": "egov.*.moj.id.*","permissions": "it.pt.pdm&fc,it.pt.pdm&fc.contract.work.*"}',
    LANDLORDPT = '{"evidences": "egov.*.moj.id.*,*.*.*.contract.work.*","permissions": "individual.pt.egov.mof,egov.pt.mof.tax.rental.contract.*"}',

    KONSTANTINOS ='{"evidences":"egov.*.moj.id.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*,egov.*.ibs.passport.*","permissions":"egov.tk.moh,egov.gr.moh,egov.tk.ibs,egov.tk.moh.disability.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*"}'
}

export const MOH_OPTIONS: DemonstratorInfo[] = [
    {
        id: "create",
        icon: "medkit",
        route: "/egov-moh/create",
    },
    {
        id: "convert",
        icon: "color-filter",
        route: "/egov-moh/convert",
    },
    {
        id: "insurance",
        icon: "medical",
        route: "/egov-moh/insurance",
    }
]
