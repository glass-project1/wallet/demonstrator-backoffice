
import { DBModel, injectable } from "@glass-project1/db-decorators";
import {IEGovId, DBandDIDHolder, RestConfig, toolkitInjectables} from "@glass-project1/glass-toolkit";

import { RestAbsTableManager } from "./RestAbsTableManager";
import {EGovId} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovId";


const DEFAULT_CONFIG_EGOVID: RestConfig = {
    baseEndpoint: "http://localhost:3001",  //"https://egov-id-{0}-dev.glass.pdmfc.com",
    baseEndpointSuffix: "/borest/egovid",
    authorizationType: undefined,
    authorizationToken: undefined,
    mode: "cors",
    pathParamsSeparator: "|",
    domain: undefined
}

const getConfig = function(country: string, config?: RestConfig) {
    const newConfig = Object.assign({}, DEFAULT_CONFIG_EGOVID, config || {});

    if(!!newConfig.baseEndpoint)
    newConfig.baseEndpoint = newConfig.baseEndpoint?.replace("{0}", country);

    return newConfig;
}

@injectable("EGovPtIdRestManager")
export class EGovPtIdRestManager extends RestAbsTableManager<IEGovId> {
  
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
        config = getConfig("pt", config)
        super(walletManager, EGovId(toolkitInjectables), "egovid", config);
    }    
}
  
@injectable("EGovGrIdRestManager")
export class EGovGrIdRestManager extends RestAbsTableManager<IEGovId> {
  
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
      config = getConfig("gr", config)
      super(walletManager, EGovId(toolkitInjectables), "egovid", config);
    }    
} 

@injectable("EGovTkIdRestManager")
export class EGovTkIdRestManager extends RestAbsTableManager<IEGovId> {
  
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
        config = getConfig("tk", config)
        super(walletManager, EGovId(toolkitInjectables), "egovid", config);
    }    
}