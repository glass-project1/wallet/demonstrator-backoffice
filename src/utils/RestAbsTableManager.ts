import { DBModel } from "@glass-project1/db-decorators";
import { RestConfig, DBModelBuilder, DefaultQueryOptions, RestManager, DBandDIDHolder, managerCallIterator, TableManager, DatabaseBuilder, NestJSRestDatabase} from "@glass-project1/glass-toolkit";
import { all, CriticalError, debugCallback, Err, errorCallback } from "@glass-project1/logging";
import { DSUDbRecord, GenericCallback } from "@glass-project1/opendsu-types";

const DEFAULT_CONFIG: RestConfig = {
    baseEndpoint: "http://localhost:8080", 
    baseEndpointSuffix: undefined,
    authorizationType: undefined,
    authorizationToken: undefined,
    mode: "cors",
    pathParamsSeparator: "|",
    domain: undefined
}

export abstract class RestAbsTableManager<T extends DBModel> extends RestManager<T>{

    protected constructor(walletManager: DBandDIDHolder, clazz: DBModelBuilder<T>, tableName: string | undefined, config: RestConfig = DEFAULT_CONFIG, database: DatabaseBuilder = NestJSRestDatabase) {
        super(walletManager, clazz, tableName, config, database);
    }

    protected mapToKey(record: DSUDbRecord, ...args: any[]): string {
        return (record as any)[Object.keys(record)[0]];
    }

    select(query: string[] | undefined, sort: "asc" | "dsc" | undefined, limit: number | undefined, mapToKey: boolean = false, ...args: (any | GenericCallback<T[] | string[]>)[]): void {   
        const callback: GenericCallback<T[] | string[]> = args.pop();
        if (!callback)
            throw new CriticalError("No callback", this);
        if (!query)
            query = DefaultQueryOptions.query;
        if (!sort)
            sort = DefaultQueryOptions.sort;
        if (typeof limit === 'undefined')
            limit = DefaultQueryOptions.limit;

        const self  = this;
        all.call(self, "Trying to query the {0} table, with the query {1}, sort {2} and limit {3}", self.tableName, query, sort, limit);
        this.db.query(this.tableName, query, sort, limit, (err, records?: any) => {
            if (err || !records || !records.results)
                return debugCallback.call(self, err || "Could not perform query {0} on table {1}", callback, query, self.tableName);

            records = records.results;

            if (mapToKey)
                return callback(undefined, records.map((r: any) => self.mapToKey(r)));
            
            managerCallIterator.call(self as TableManager<T>, self.instantiateModel.bind(self), records, (err: Err, models: T[]) => {
                if (err || !models)
                    return errorCallback.call(self, err || "No models instantiated", callback);
                
                all.call(self, "Query on the {0} table successful. Retrieved {1} records", self.tableName, models.length);
                callback(undefined, models);
            });
        });
    }

    instantiateModel(record: any | T, ...args: (any | GenericCallback<T>)[]): void {
        const callback: GenericCallback<T> = args.pop();
        if (!callback)
            throw new CriticalError("No callback", this);
        if (this.clazz)
            return callback(undefined, new this.clazz(record) as T);
        callback(undefined, record as T);
    }

    read(key: string | number, ...args: (any | GenericCallback<T | DSUDbRecord>)[]): void {
        const callback: GenericCallback<T> = args.pop();
        if (!callback)
            throw new CriticalError("No callback", this);
        const self  = this;
        all.call(self, "Trying to read record under the {0} table, with the key {1}", this.tableName, key);
        this.db.getRecord(this.tableName, key, (err, record?: any) => {
            if (err || !record)
                return debugCallback.call(self, err || "Could not read record with key {0}", callback, key);
            all.call(self, "New record read under the {0} table, with the key {1}", this.tableName, key);
            self.instantiateModel(record as any | T, callback);
        });
    }

    create(key: string | number | undefined | T, model: T | any, ...args: (any | GenericCallback<T | DSUDbRecord>)[]): void {
        const callback: GenericCallback<T | DSUDbRecord> = args.pop();
        if (!callback)
            throw new CriticalError("No callback", this);
        const self  = this;

        all.call(self, "Trying to create a new record under the {0} table, with the key {1} and value {2}", this.tableName, key, model);
        this.db.insertRecord(this.tableName, key as string| number, self.indexModel(model), (err, record?: DSUDbRecord) => {
            if (err || !record)
                return debugCallback.call(self, err || "Could not create record with key {0}", callback, key);
            all.call(self, "New record created under the {0} table, with the key {1} and value {2}", this.tableName, key, model);
            self.instantiateModel(record as any | T, callback);
        });
    }
}