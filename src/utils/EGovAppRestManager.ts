import { DBModel, injectable } from "@glass-project1/db-decorators";
import { DBandDIDHolder, RestConfig } from "@glass-project1/glass-toolkit";
import { EGovEntity } from "./EGovEntity";

import { RestAbsTableManager } from "./RestAbsTableManager";

const DEFAULT_CONFIG_EGOVAPP: RestConfig = {
    baseEndpoint: "http://localhost:3000", //"https://egov-{0}-dev.glass.pdmfc.com",
    baseEndpointSuffix: "/borest/egov",
    authorizationType: undefined,
    authorizationToken: undefined,
    mode: "cors",
    pathParamsSeparator: "|",
    domain: undefined
}

const getConfig = function(country: string, config?: RestConfig) {
    const newConfig = Object.assign({}, DEFAULT_CONFIG_EGOVAPP, config || {});

    if(!!newConfig.baseEndpoint)
        newConfig.baseEndpoint = newConfig.baseEndpoint?.replace("{0}", country);

    return newConfig;
}

@injectable("EGovPtAppRestManager")
export class EGovPtAppRestManager extends RestAbsTableManager<EGovEntity> {
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
        config =  getConfig("pt", config)
        super(walletManager, EGovEntity, "egov", config);
    }     
}
@injectable("EGovGrAppRestManager")
export class EGovGrAppRestManager extends RestAbsTableManager<EGovEntity> {
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
      config =  getConfig("gr", config)
      super(walletManager, EGovEntity, "egov", config);
    }     
}
@injectable("EGovTkAppRestManager")
export class EGovTkAppRestManager extends RestAbsTableManager<EGovEntity> {
    constructor(walletManager: DBandDIDHolder & DBModel, config?: RestConfig){
      config =  getConfig("tk", config)
      super(walletManager, EGovEntity, "egov", config);
    }     
}

