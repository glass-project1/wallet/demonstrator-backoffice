import type {
  ICamera,
  IClipboard,
  IDevice,
  IFsStorage,
  IHaptics,
  ILocation,
  INative, IStorage, IBiometric, IBarcodeScanner, NativeCapabilities,
} from '@glass-project1/native-interfaces';
import {
  EnvironmentDefinition
} from '@glass-project1/opendsu-types';
import {injectable} from "@glass-project1/db-decorators";
import { LoggedError, errorCallback, CriticalError } from '@glass-project1/logging';
import { NativeKey } from "@glass-project1/base-web-components";

@injectable("Native")
export class Native implements INative {

  private readonly native: INative;

  public readonly Environment!: EnvironmentDefinition;
  public readonly Capabilities!: () => Promise<NativeCapabilities>;

  public readonly Camera!: ICamera;
  public readonly Clipboard!: IClipboard;
  public readonly Device!: IDevice;
  public readonly Filesystem!: IFsStorage;
  public readonly Haptics!: IHaptics;
  public readonly Location!: ILocation;
  public readonly Storage!: IStorage;
  public readonly Biometrics!: IBiometric;
  public readonly BarcodeScanner!: IBarcodeScanner;

  constructor(native?: INative) {
    this.native = native || globalThis.window[NativeKey];
    if (!this.native)
      throw new LoggedError("Failed to retrieve native object");
    this.init();
  }

  private init(){
    const self: Native = this;
    Object.defineProperty(self, "Environment", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Environment
    })

    Object.defineProperty(self, "Capabilities", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Capabilities
    })


    Object.defineProperty(self, "Biometrics", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Biometrics
    })

    Object.defineProperty(self, "BarcodeScanner", {
      enumerable: true,
      configurable: false,
      get: () => self.native.BarcodeScanner
    })

    Object.defineProperty(self, "Camera", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Camera
    })

    Object.defineProperty(self, "Clipboard", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Clipboard
    })

    Object.defineProperty(self, "Device", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Device
    })

    Object.defineProperty(self, "Filesystem", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Filesystem
    })

    Object.defineProperty(self, "Haptics", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Haptics
    })

    Object.defineProperty(self, "Location", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Location
    })

    Object.defineProperty(self, "Storage", {
      enumerable: true,
      configurable: false,
      get: () => self.native.Storage
    })
  }
}


export function getNativeCodeScanner(win: Window){
  const document: Document = win.document;
  if (!document)
    throw new CriticalError("no document found", getNativeCodeScanner);
  const scanner: HTMLGlassNativeCodeReaderElement = document.getElementById("native-scanner") as HTMLGlassNativeCodeReaderElement;
  if (scanner)
    return scanner;
  if (win.parent === win)
    throw new CriticalError("reached the end of the windows and no scanner was found");
  return getNativeCodeScanner(win.parent);
}

export async function scanNative(win?: Window){
  return new Promise<string>((resolve, reject) => {
    if (!win)
      win = globalThis.window as Window;
    if (!win)
      return errorCallback.call(scanNative, "Failed to get window", reject);
    let scanner: HTMLGlassNativeCodeReaderElement;
    try {
      scanner = getNativeCodeScanner(win);
    } catch (e: any) {
      return errorCallback.call(scanNative, "Failed to get Native Code Scanner Element: {0}", reject, e);
    }
    (scanner.scan() as Promise<string>).then(resolve).catch(reject)
  })
}
