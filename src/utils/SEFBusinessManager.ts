import {NestJSRestDatabase, RestConfig, RestDatabase} from "@glass-project1/glass-toolkit";
import {Callback} from "@glass-project1/logging";
import {injectable} from "@glass-project1/db-decorators";

@injectable("SEFBusinessManager")
export class SEFBusinessManager {

    readonly config: RestConfig

    constructor(config?: RestConfig) {
        this.config = config || {
            baseEndpoint: "http://localhost:3002", //"https://egov-passport-{0}-dev.glass.pdmfc.com",
            baseEndpointSuffix: "", //"/borest/egovid",
            authorizationType: undefined,
            authorizationToken: undefined,
            mode: "cors",
            pathParamsSeparator: "|",
            domain: undefined
        };
    }

    requestResidencePermitEvidences(did: string, country: string = "pt", callback: Callback){
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
     
        new NestJSRestDatabase(config).insertRecord("sef", did, {did: did}, callback)
    }

    requestTempResidencePermitEvidences(did: string, country: string = "pt", callback: Callback){
        const config = Object.assign({}, this.config);
        config.baseEndpoint = config.baseEndpoint?.replace("{0}", country);
     
        new NestJSRestDatabase(config).insertRecord("seftemp", did, {did: did}, callback)
    }
}