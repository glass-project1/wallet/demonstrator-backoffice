import {NestJSRestDatabase, RestConfig, RestDatabase} from "@glass-project1/glass-toolkit";
import {Callback} from "@glass-project1/logging";
import {injectable} from "@glass-project1/db-decorators";

@injectable("PDMFCBusinessManager")
export class PDMFCBusinessManager {

    readonly config: RestConfig

    constructor(config?: RestConfig) {
        this.config = config || {
            baseEndpoint: "http://localhost:8081",
            baseEndpointSuffix: undefined,
            authorizationType: undefined,
            authorizationToken: undefined,
            mode: "cors",
            pathParamsSeparator: "|",
            domain: undefined
        };
    }

    issueWorkContract(did: string, callback: Callback){
        new NestJSRestDatabase(this.config).insertRecord("work-contract", did, {did: did}, callback)
    }
}