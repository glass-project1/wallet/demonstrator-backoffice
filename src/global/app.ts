import '@ionic/core';
import '@glass-project1/ui-decorators-web';
import '@glass-project1/base-web-components';
import { debug, Err, errorCallback, getLogger, LoggedError, LOGGER_LEVELS } from '@glass-project1/logging';
import {getDataStore, getGlassModal, getGlassSpinner, getRouter, GlassModal} from '@glass-project1/base-web-components';
import { EGovWalletManager, LandlordBusinessManager, Native, PDMFCBusinessManager, SEFBusinessManager, MoJBusinessManager, EGovPtIdRestManager, EGovGrIdRestManager, EGovTkIdRestManager, EGovPtAppRestManager, EGovGrAppRestManager, EGovTkAppRestManager} from '../utils';
import {
  GlassInputHandler,
  GlassRequest,
  GlassRequestTypes, IGlassRequestHandler,
  InputHandlerMethod, InputType,
  RequestHandlerRegistry,
  RestConfig,
  setSpinner
} from '@glass-project1/glass-toolkit';
import { FileService } from '@glass-project1/localization';
import { BankRestManager } from '../utils/bank';
import { DisabilityReportBusinessManager } from '../utils/DisabilityReportBusinessManager';
import {bindNative} from "@glass-project1/native-integration";
import {GenericCallback} from "@glass-project1/opendsu-types";


class Booter {

  private eGovWalletManager: EGovWalletManager;

  public initialize(): Promise<void>{
    const self: Booter = this;

    return new Promise<void>(async (resolve, reject) => {
      try{
        getRouter();
        getDataStore();
        getLogger().setLevel(LOGGER_LEVELS.DEBUG);
        self.loadWalletManager();
        self.initializeWallet();
        // const native: Native = self.bootNativeIntegration();
      }catch (e: any) {
        return errorCallback.call(self, e, reject);
      }
      resolve();
    })
  }

  private loadWalletManager(){
    const self: Booter = this;

    const egovWalletM: EGovWalletManager = new EGovWalletManager({});

    if(!egovWalletM)
      throw new LoggedError("Failed to create Egov Wallet Manager", self, LOGGER_LEVELS.ERROR); 

    self.eGovWalletManager = egovWalletM;
  }

  private async getWalletInputModal(inputMode: InputType = InputType.CODE, presentingElement?: HTMLElement){
    const modal: GlassModal = getGlassModal();

    if (!presentingElement)
      try {
        presentingElement = window.parent.document.querySelector("ion-router") as HTMLElement;
      } catch (e) {
        throw new LoggedError("Could not find presenting Element for the glass Modal")
      }

    if (!presentingElement)
      throw new LoggedError("Could not find presenting Element for the glass Modal")
    return modal.present("glass-wallet-input", presentingElement,{
      "input-mode": inputMode
    })
  }

  private async getWalletInput(type: InputType, elm?: HTMLElement){
    const self = this;
    return new Promise<string | undefined>(async (resolve, reject) => {
      self.getWalletInputModal(type, elm)
          .catch(reject)
          .then((modal: GlassModal | void) => {
            if (!modal)
              return reject("No modal received");
            modal.onDismiss((data, action) => {
              if (action === "cancel")
                return resolve(undefined);
              resolve(data);
            })
          })
    })
  }

  private getWalletInputHandler(){

    const responser: {[indexer: string]: InputHandlerMethod} = {
      code: async () => this.getWalletInput(InputType.CODE),
      input: async () => this.getWalletInput(InputType.INPUT)
    }

    return class implements IGlassRequestHandler<string> {
      handle(request: GlassRequest<any>, callback: GenericCallback<string>): void {
        callback("Not implemented")
      }
      handleAsync(request: GlassRequest<any>): Promise<string> {
        return Promise.reject("Not implemented")
      }
      constructor() {
        return new GlassInputHandler(responser);
      }
    }
  }

  private bootNativeIntegration() {
    let self: Booter = this;
    debug.call(self.bootNativeIntegration, "Loading native features");
    return new Native();
  }

  private async initializeWallet(){
    const self: Booter = this;

    let fileService: FileService = new FileService({path: "assets/endpoints/"})

    return new Promise<void>(async (resolve, reject) => {
      fileService.getFile("egov", (err: Err, egovConfig: any) => {
        if(err)
          debug.call(self, "Unable to get egov json");

        fileService.getFile("egovid", (err: Err, egovIdConfig: any) => {
          if(err)
            debug.call(self, "Unable to get egov id json");

          fileService.getFile("egovpassport", (err: Err, egovPassportConfig: any) => {
            if (err)
              debug.call(self, "Unable to get egov id json");

            fileService.getFile("pdmfc", (err: Err, pdmfcConfig: any) => {
              if (err)
                debug.call(self, "Unable to get pdmfc json");

              fileService.getFile("bank", (err: Err, bankConfig: any) => {
                if (err)
                  debug.call(self, "Unable to get bank json");

                fileService.getFile("landlord", (err: Err, landlordConfig: any) => {
                  if (err)
                    debug.call(self, "Unable to get landlord json");

                  fileService.getFile("disability", (err: Err, disabilityConfig: any) => {
                    if (err)
                      debug.call(self, "Unable to get disability json");

                    setSpinner(getGlassSpinner());

                    if (!self.eGovWalletManager)
                      throw new LoggedError("Egov Wallet Manager doens't exist", self, LOGGER_LEVELS.ERROR);

                    try {

                      const eGovPtIdRestManager: EGovPtIdRestManager = new EGovPtIdRestManager(self.eGovWalletManager, egovIdConfig ? egovIdConfig : undefined);
                      const eGovGrIdRestManager: EGovGrIdRestManager = new EGovGrIdRestManager(self.eGovWalletManager, egovIdConfig ? egovIdConfig : undefined);
                      const eGovTkIdRestManager: EGovTkIdRestManager = new EGovTkIdRestManager(self.eGovWalletManager, egovIdConfig ? egovIdConfig : undefined);

                      const eGovPtRestManager: EGovPtAppRestManager = new EGovPtAppRestManager(self.eGovWalletManager, egovConfig ? egovConfig : undefined);
                      const eGovGrRestManager: EGovGrAppRestManager = new EGovGrAppRestManager(self.eGovWalletManager, egovConfig ? egovConfig : undefined);
                      const eGovTkRestManager: EGovTkAppRestManager = new EGovTkAppRestManager(self.eGovWalletManager, egovConfig ? egovConfig : undefined);

                      const bankRestManager: BankRestManager = new BankRestManager(self.eGovWalletManager, bankConfig ? bankConfig : undefined);
                      const pdmfcRestManager: PDMFCBusinessManager = new PDMFCBusinessManager(pdmfcConfig ? pdmfcConfig : undefined);
                      const mojBusinessManager: MoJBusinessManager = new MoJBusinessManager(egovIdConfig ? egovIdConfig : undefined);
                      const landlordBusinessManager: LandlordBusinessManager = new LandlordBusinessManager(landlordConfig ? landlordConfig : undefined);
                      const sefBusinessManager: SEFBusinessManager = new SEFBusinessManager(egovPassportConfig ? egovPassportConfig : undefined);
                      const disabilityReportBusinessManager: DisabilityReportBusinessManager = new DisabilityReportBusinessManager(disabilityConfig ? disabilityConfig : undefined);

                      const rh = new RequestHandlerRegistry();
                      bindNative(globalThis.window as Window)
                      this.bootNativeIntegration();

                      rh.registerRequestHandler(GlassRequestTypes.INPUT, this.getWalletInputHandler());

                    } catch (e: any) {
                      return errorCallback.call(self, e, reject);
                    }

                    resolve();
                  })
                })
              })
            });
          });
        })
      })

    })


  }
}

export default async (): Promise<void> => {
  return (new Booter()).initialize();
};