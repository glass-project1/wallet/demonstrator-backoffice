import { GlassLocaleService } from '@glass-project1/base-web-components';
import { newSpecPage } from '@stencil/core/testing';
import { AppPdmfc } from '../app-pdmfc';

describe('app-pdmfc', () => {

  beforeAll(() => {
    const ls = new GlassLocaleService({});
  })


  it('renders', async () => {
    const page = await newSpecPage({
      components: [AppPdmfc],
      html: `<app-pdmfc></app-pdmfc>`,
    });
    
    const config = {
      backroute: "/",
      icon: "briefcase-outline",
      locale: "requests.issue-work-contract"
    };
    
    expect(page.root).toEqualHtml(`
      <app-pdmfc>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="${config.backroute}" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>title</ion-title>
            </ion-toolbar>
          </ion-header>

          <ion-content fullscreen="">
            <div class="d-content-section">
              
              <ion-item lines="none">
                <div>
                  <p>subtitle</p>
                  <h1>title</h1>
                </div>
              </ion-item>

              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name=${config.icon}></ion-icon>
                    ${config.locale}.title
                  </ion-card-title>
                  <ion-card-subtitle>${config.locale}.subtitle</ion-card-subtitle>
                </ion-card-header>
                
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>${config.locale}.button</ion-label>
                  </ion-button>
                </div>
              </ion-card>   
                
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-pdmfc>
    `);
  });
});
