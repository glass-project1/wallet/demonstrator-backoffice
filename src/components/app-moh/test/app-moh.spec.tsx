import { newSpecPage } from '@stencil/core/testing';
import { AppMoh } from '../app-moh';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-moh', () => {
  it('renders', async () => {
    const ts = new GlassLocaleService({});
    const page = await newSpecPage({
      components: [AppMoh],
      html: `<app-moh></app-moh>`,
    });
    expect(page.root).toEqualHtml(`
      <app-moh>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="/countries/egov-moj" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>
                title
              </ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content fullscreen="">
            <div class="d-content-section">
              <ion-item lines="none">
                <div>
                  <p>
                    subtitle
                  </p>
                  <h1>
                    description
                  </h1>
                </div>
              </ion-item>
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="medkit"></ion-icon>
                    requests.create.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    requests.create.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>
                      requests.create.button
                    </ion-label>
                  </ion-button>
                </div>
              </ion-card>
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="color-filter"></ion-icon>
                    requests.convert.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    requests.convert.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>
                      requests.convert.button
                    </ion-label>
                  </ion-button>
                </div>
              </ion-card>
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="medical"></ion-icon>
                    requests.insurance.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    requests.insurance.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>
                      requests.insurance.button
                    </ion-label>
                  </ion-button>
                </div>
              </ion-card>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-moh>
    `);
  });
});
