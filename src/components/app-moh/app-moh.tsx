import { Component, State, h } from '@stencil/core';
import {GlassLocaleService, GlassRouter, injectAndLocalize} from "@glass-project1/base-web-components";
import {inject} from "@glass-project1/db-decorators";
import {DemonstratorInfo} from "../../utils";
import {MOH_OPTIONS} from "../../utils/constants";
import { stringFormat } from '@glass-project1/logging';
import {ICountry, getCountry} from "../../utils/utils";


@Component({
  tag: 'app-moh',
  styleUrl: 'app-moh.scss',
  shadow: true,
})
export class AppMoh {

  /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
  @injectAndLocalize("moh", "GlassLocaleService")
  locale!: GlassLocaleService;

  @inject("GlassRouter")
  router!: GlassRouter;

  @State()
  description: string = "";

  country?: ICountry;
  
  handleSelectCountry(demonstrator:{[indexer: string]: any}) {
    const self = this;
    self.router.push(`${demonstrator.route}/${this.country?.name.toLowerCase()}`)
  }

  componentWillLoad() {
    this.description = this.locale.get("description") as string;

    if(!this.country)
      this.country = getCountry() as ICountry;

    this.description = stringFormat(this.description, !!this.country ? this.country.name : "");
  }
 
  private getButton(demonstrator: DemonstratorInfo){
    const self = this;
    return(
      <ion-button onClick={() => self.handleSelectCountry(demonstrator)}>
        <ion-icon name='chevron-forward-outline' slot='end'></ion-icon>
        <ion-label>{this.locale.get("requests." + demonstrator.id + ".button")}</ion-label>
      </ion-button>
    );
  }

  generateDemonstratorCard(demonstrator: DemonstratorInfo): any{
    const self = this;

    return (
      <ion-card>
        <ion-card-header>
          <ion-card-title>
            <ion-icon name={demonstrator.icon}></ion-icon>
            {this.locale.get(`requests.${demonstrator.id}.title`)}
          </ion-card-title>
          <ion-card-subtitle>{this.locale.get(`requests.${demonstrator.id}.subtitle`)}</ion-card-subtitle>
        </ion-card-header>
        <div class="d-card-footer">
          {self.getButton(demonstrator)}
        </div>
      </ion-card>   
    )
  }

  getContent(){
    const self = this;

    return MOH_OPTIONS.slice(0, MOH_OPTIONS.length).map((d) => self.generateDemonstratorCard(d));
  }

  render() {
    const self = this;

    return [
        <ion-header collapse="fade" class="ion-no-border">
          <ion-toolbar color="primary">
            <ion-buttons slot="start">
              <ion-back-button defaultHref="/countries/egov-moj" icon="chevron-back-outline"></ion-back-button>
            </ion-buttons>
            <ion-title>{this.locale.get("title")}</ion-title>
          </ion-toolbar>
        </ion-header>,
        <ion-content fullscreen={true}>
          <div class="d-content-section">
            <ion-item lines="none">
              <div>
                <p>{this.locale.get("subtitle")}</p>
                <h1>{this.description}</h1>
              </div>
            </ion-item>
            {self.getContent()}
          </div>
        </ion-content>
    ];
  }

}
