import { newSpecPage } from '@stencil/core/testing';
import { GlassWalletInput } from '../glass-wallet-input';
import { GlassLocaleService } from '@glass-project1/base-web-components';



describe('glass-wallet-input', () => {

  beforeAll(() => {
    const ls = new GlassLocaleService({});
  })

  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassWalletInput],
      html: `<glass-wallet-input></glass-wallet-input>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-wallet-input input-mode="code">
        <ion-header class="ion-no-border">
          <ion-toolbar color="tertiary">
            <ion-buttons slot="start">
              <ion-button fill="clear">
                <ion-icon name="chevron-back-outline" slot="icon-only"></ion-icon>
              </ion-button>
            </ion-buttons>
            <ion-title>
              code.title
            </ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <glass-code-scanner scanner-description="code.scanner-description"></glass-code-scanner>
          <ion-segment color="primary" value="code">
            <ion-segment-button value="code">
              <ion-label>
                code.button
              </ion-label>
            </ion-segment-button>
            <ion-segment-button value="input">
              <ion-label>
                input.button
              </ion-label>
            </ion-segment-button>
          </ion-segment>
        </ion-content>
      </glass-wallet-input>
    `);
  });
});
