import {Component, Element, h, Prop, State} from '@stencil/core';
import {getGlassModal, GlassModal, GlassLocaleService, injectAndLocalize, CodeScannerStatus, getGlassToast} from "@glass-project1/base-web-components";
import {debug, error} from "@glass-project1/logging";
import {INative} from "@glass-project1/native-interfaces";
import {inject} from "@glass-project1/db-decorators";
import {SegmentChangeEventDetail} from "@ionic/core";
import { InputType } from '@glass-project1/glass-toolkit';

@Component({
  tag: 'glass-wallet-input',
  styleUrl: 'glass-wallet-input.scss'
})
export class GlassWalletInput {

  @Element()
  element: HTMLGlassInputElement;

  @injectAndLocalize("input", "GlassLocaleService")
  locale: GlassLocaleService;

  @inject("Native")
  Native: INative;

  /**
   * Defines the input mode
   */
  @Prop({attribute: 'input-mode', mutable: true, reflect: true}) inputMode: InputType = InputType.CODE;

  @Prop({attribute: 'show-options'}) showOptions: boolean = true;

  @State()
  result: string = "";

  @State()
  confirmResult: string = "";

  @State()
  visiblePasswordSlotOne: boolean = false;

  @State()
  visiblePasswordSlotTwo: boolean = false;

  private input: HTMLGlassInputElement;

  private modal: GlassModal;

  private scanner: HTMLGlassCodeScannerElement;

  componentWillRender(){
    this.modal = getGlassModal();

    if(this.inputMode === InputType.PASSWORD)
      this.showOptions = false;

    if (!this.modal)
      error.call(this, "No Glass Modal Found")
  }

  private getCodeScanner(){
    return (
      <glass-code-scanner
        scanner-description={this.locale.get(`${InputType.CODE}.scanner-description`)}
        onScanResult={e => this.setValue.call(this, e.detail, false)}
        ref={el => this.scanner = el as HTMLGlassCodeScannerElement}>
      </glass-code-scanner>)
  }

  private setValue(value: string, awaitConfirmation: boolean = true){
    this.result = value;
    if (awaitConfirmation)
      return;

    const self = this;

    this.submitValue().then(_ => debug.call(self, "Result submitted: {0}", this.result));
  }

  private async submitValue(){
    await this.stopScanner();
    await this.modal.submit(this.result);
  }

  private async submitPassword(){
    if(this.confirmResult !== this.result){
      await getGlassToast().error(this.locale.get("password.errors.password-match") as string);
      return;
    }

    await getGlassToast().success(this.locale.get("password.success.success") as string)
    await this.modal.submit(this.result);
  }

  private async closeModal(){
    await this.stopScanner();
    await this.modal.dismiss();
  }


  private getInput(){
    const self = this;
    const pasteValue = async function(){
      const contents = await self.Native.Clipboard.read();
      self.setValue(contents.value);
    }

    return (
      <div class="g-code">
        <ion-item class="g-code__item">
          <glass-input label={this.locale.get(`${InputType.INPUT}.label`) as string}
                       placeholder={this.locale.get(`${InputType.INPUT}.placeholder`) as string}
                       ref={el => this.input = el as HTMLGlassInputElement}
                       value={this.result}
                       onInput= {(evt) => self.result = (evt.target as HTMLInputElement).value}
          ></glass-input>
          <ion-button fill="clear" size="default" slot="end" onClick={pasteValue}>
            <ion-icon name="clipboard-outline"></ion-icon>
          </ion-button>
        </ion-item>
        <ion-buttons class="ion-align-items-center">
          <ion-button onClick={self.submitValue.bind(self)}>
            {this.locale.get(`${InputType.INPUT}.submit`) as string}
          </ion-button>
        </ion-buttons>
      </div>

    )
  }


  private getPassword(){
    const self = this;
    const pasteValue = async function(){
      const contents = await self.Native.Clipboard.read();
      self.setValue(contents.value);
    }

    return (
      <div class="g-code">
        <ion-item class="g-code__item">
          <glass-input label={this.locale.get(`${InputType.PASSWORD}.label`) as string}
                       placeholder={this.locale.get(`${InputType.PASSWORD}.placeholder`) as string}
                       ref={el => this.input = el as HTMLGlassInputElement}
                       value={this.result}
                       type={this.visiblePasswordSlotOne ? "text" : "password"}
                       onInput= {(evt) => self.result = (evt.target as HTMLInputElement).value}
          ></glass-input>
          <ion-buttons slot='end'>
            <ion-button fill="clear" size="default" slot="end" onClick={() => {self.visiblePasswordSlotOne = !self.visiblePasswordSlotOne}}>
              <ion-icon name={self.visiblePasswordSlotOne ? "eye-off-outline" : "eye-outline"}></ion-icon>
            </ion-button>
            <ion-button fill="clear" size="default" slot="end" onClick={pasteValue}>
              <ion-icon name="clipboard-outline"></ion-icon>
            </ion-button>
          </ion-buttons>
        </ion-item>
        <ion-item class="g-code__item">
          <glass-input label={this.locale.get(`${InputType.PASSWORD}.confirm-label`) as string}
                       placeholder={this.locale.get(`${InputType.PASSWORD}.placeholder`) as string}
                       value={this.confirmResult}
                       type={this.visiblePasswordSlotTwo ? "text" : "password"}
                       onInput= {(evt) => self.confirmResult = (evt.target as HTMLInputElement).value}
          ></glass-input>
          <ion-buttons slot='end'>
            <ion-button fill="clear" size="default" slot="end" onClick={() => {self.visiblePasswordSlotTwo = !self.visiblePasswordSlotTwo}}>
              <ion-icon name={self.visiblePasswordSlotTwo ? "eye-off-outline" : "eye-outline"}></ion-icon>
            </ion-button>
            <ion-button fill="clear" size="default" slot="end" onClick={pasteValue}>
              <ion-icon name="clipboard-outline"></ion-icon>
            </ion-button>
          </ion-buttons>
        </ion-item>
        <ion-buttons class="ion-align-items-center">
          <ion-button onClick={self.submitPassword.bind(self)}>
            {this.locale.get(`${InputType.INPUT}.submit`) as string}
          </ion-button>
        </ion-buttons>
      </div>

    )
  }

  private getInputByMode(){
    switch (this.inputMode) {
      case InputType.CODE:
        return this.getCodeScanner();
      case InputType.INPUT:
        return this.getInput();
      case InputType.PASSWORD:
        return this.getPassword();
      default:
        error.call(this, "Invalid input mode: {0}", this.inputMode);
    }
  }

  private async stopScanner(){
    if (!this.scanner)
      return debug.call(this, "No scanner found to stop...");

    const status: CodeScannerStatus = await this.scanner.getStatus();
    if (status !== CodeScannerStatus.DONE)
      try {
        await this.scanner.stop();
      } catch (e: any) {
        error.call(this, "Failed to stop camera: {0}", e.message || e);
      }
  }

  private async changeMode(evt: CustomEvent<SegmentChangeEventDetail>){
    const {value} = evt.detail;
    if (!value || !Object.values(InputType).includes(value as InputType))
      return error.call(this, "Received mode is not supported");
    if (value !== InputType.CODE)
      await this.stopScanner();
    this.inputMode = value as InputType;
  }

  private getButtons(){
    if(!this.showOptions) return;

    return(
        <ion-segment color="primary" value={this.inputMode} onIonChange={this.changeMode.bind(this)}>
          <ion-segment-button value={InputType.CODE}>
            <ion-label>{this.locale.get(`${InputType.CODE}.button`)}</ion-label>
          </ion-segment-button>
          <ion-segment-button value={InputType.INPUT}>
            <ion-label>{this.locale.get(`${InputType.INPUT}.button`)}</ion-label>
          </ion-segment-button>
        </ion-segment>
    )
  }

  render() {
    return [
      <ion-header class="ion-no-border">
        <ion-toolbar color="tertiary">
          <ion-buttons slot="start">
            <ion-button fill="clear" onClick={this.closeModal.bind(this)}>
              <ion-icon slot="icon-only" name="chevron-back-outline"></ion-icon>
            </ion-button>
          </ion-buttons>
          <ion-title>{this.locale.get(`${this.inputMode}.title`)}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        {this.getInputByMode()}
        {this.getButtons()}
      </ion-content>
    ];
  }
}
