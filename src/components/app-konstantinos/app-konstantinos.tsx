import { Component, Host, Prop, h } from '@stencil/core';
import { Native, RequestInfo } from '../../utils';
import { inject } from '@glass-project1/db-decorators';
import { GlassLocaleService, getGlassToast, injectAndLocalize } from '@glass-project1/base-web-components';

@Component({
  tag: 'app-konstantinos',
  styleUrl: 'app-konstantinos.scss',
  shadow: true,
})
export class AppKonstantinos {

  @inject("Native")
  native!: Native;

  @injectAndLocalize("konstantinos", "GlassLocaleService")
  locale!: GlassLocaleService;


  data?: string;

  
  header?: string;

 
  label?: string;


  componentWillLoad(){
    const self: AppKonstantinos = this;

    self.data = RequestInfo.KONSTANTINOS;
    self.header = self.locale.get("header") as string,
    self.label = self.locale.get("label") as string
  }

  private getQRCode(){
    const self = this;

    return (
      <div>
        <div class="g-code-container">
          <div>
            <div class="g-code-generator">
              <glass-code-generator data={self.data}></glass-code-generator>
            </div>
          </div>
        </div>
        <div class="g-did-container">
          <glass-input lines="none" labelPosition="stacked" readonly={true} label={self.label} value={self.data}></glass-input>
            <ion-button onClick={self.copyData.bind(self)}>
              <ion-icon name='clipboard'></ion-icon>
            </ion-button>
        </div>
      </div>
    )
  }

  private copyData() {
    const self = this;
    self.native.Clipboard.write( {string: self.data as string})
    .then(() => { 
      getGlassToast().inform("Data was successfully copied")

    })
    .catch((e) => getGlassToast().error("Failed to copy data"));
  }


  render() {
    const self: AppKonstantinos = this;
    return [
      <ion-header collapse="fade" class="ion-no-border">
      <ion-toolbar color="primary">
        <ion-buttons slot="start">
          <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
        </ion-buttons>
        <ion-title>{self.header}</ion-title>
      </ion-toolbar>
    </ion-header>,
      <ion-content>
        <div class="g-content-section">
          <ion-card>
            <ion-card-content>
              {self.getQRCode()}
            </ion-card-content>
          </ion-card>
        </div>

      </ion-content>
    ];
  }

}
