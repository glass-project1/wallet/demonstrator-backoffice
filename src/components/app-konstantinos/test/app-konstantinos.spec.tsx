import { newSpecPage } from '@stencil/core/testing';
import { AppKonstantinos } from '../app-konstantinos';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-konstantinos', () => {
  it('renders', async () => {
    const ts = new GlassLocaleService({});

    const page = await newSpecPage({
      components: [AppKonstantinos],
      html: `<app-konstantinos></app-konstantinos>`,
    });

    expect(page.root).toEqualHtml(`
      <app-konstantinos>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
          <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="/" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>
                header
              </ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content>
            <div class="g-content-section">
              <ion-card>
                <ion-card-content>
                  <div>
                    <div class="g-code-container">
                      <div>
                        <div class="g-code-generator">
                          <glass-code-generator data="{&quot;evidences&quot;:&quot;egov.*.moj.id.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*,egov.*.ibs.passport.*&quot;,&quot;permissions&quot;:&quot;egov.tk.moh,egov.gr.moh,egov.tk.ibs,egov.tk.moh.disability.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*&quot;}"></glass-code-generator>
                        </div>
                      </div>
                    </div>
                    <div class="g-did-container">
                      <glass-input label="label" labelposition="stacked" lines="none" readonly="" value="{&quot;evidences&quot;:&quot;egov.*.moj.id.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*,egov.*.ibs.passport.*&quot;,&quot;permissions&quot;:&quot;egov.tk.moh,egov.gr.moh,egov.tk.ibs,egov.tk.moh.disability.*,egov.gr.moh.disability.*,egov.tk.moh.health.*,egov.tk.ibs.permits.residence.*&quot;}"></glass-input>
                      <ion-button>
                        <ion-icon name="clipboard"></ion-icon>
                      </ion-button>
                    </div>
                  </div>
                </ion-card-content>
              </ion-card>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-konstantinos>
    `);
  });
});
