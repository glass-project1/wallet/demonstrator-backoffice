import {Component, h, State} from '@stencil/core';
import { errorCallback } from "@glass-project1/logging";
import {LocaleService} from "@glass-project1/localization";
import {getGlassLocaleService} from "@glass-project1/base-web-components";

/**
 * Entry component to the stencil app
 *
 * should handle the routing (if you are using the router implementation for navigation)
 * and should handle all initialization that is not handled in the global script
 *
 * @class AppRoot
 *
 * results in a {@link HTMLAppRootElement}
 */
@Component({
  tag: 'app-root',
  styleUrl: 'app-root.scss'
})
export class AppRoot {

  @State()
  private rootPath: string = "/"

  private locale!: LocaleService;


  async handleBaseElement () {
    const self = this;

    return new Promise<void>(async (resolve, reject) => {
      if(globalThis.window && globalThis.window.location)
        self.rootPath = globalThis.location.pathname;
      resolve();
    })
  }

  async componentWillLoad(){
    const self = this;

    return new Promise<void>(async (resolve, reject) => {
      try {
        await self.handleBaseElement();
        self.locale = await getGlassLocaleService("en");
      } catch (e: any){
        return errorCallback.call(self, "Could not initialze demonstrator UI: {0}", reject, e.message || e);
      }

      if(!self.locale)
        return errorCallback.call(self, "Failed to Boot Localization service", reject);

      resolve();
    })
  }

  render() {
    return (
      <ion-app>
        <ion-router root={this.rootPath} useHash={false}>
          <ion-route url="/" component="app-home" />

          <ion-route url="/countries" component="app-egov-countries" /> 
          <ion-route url="/egov" component="app-egov" />

          <ion-route url="/egov-moj" component="app-moj" />
          <ion-route url="/egov-moj/social" component="app-moj-social" />
          <ion-route url="/egov-moj/seftemp" component="app-moj-temp" />

          <ion-route url="/egov-moh" component="app-moh" />
          <ion-route url="/egov-moh/create" component="app-moh-create" />
          <ion-route url="/egov-moh/convert" component="app-moh-convert" />
          <ion-route url="/egov-moh/insurance" component="app-moh-insurance" />
     
          <ion-route url="/egov-moj/social/:did" component="app-moj-social" />
          <ion-route url="/bank" component="app-bank" />
          <ion-route url="/bank/open" component="app-bank-open" />
          <ion-route url="/pdmfc" component="app-pdmfc" />
          <ion-route url="/pdmfc/issue-work-contract" component='app-pdmfc-work-contract'/>
          <ion-route url="/landlord" component="app-landlord" />
          <ion-route url="/landlord/issue-contract" component='app-landlord-issue-contract'/>
          <ion-route url="/sef/permit" component='app-sef-permit'/>
          <ion-route url="/konstantinos" component='app-konstantinos'/>
          {/* <ion-route url="/disability" component="app-disability" />
          <ion-route url="/disability/issue-report" component="app-disability-issue-report" />   */}
          <ion-route url="/signon" component="app-single-sign-on" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}