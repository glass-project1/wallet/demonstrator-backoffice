import { GlassLocaleService } from '@glass-project1/base-web-components';
import { newSpecPage } from '@stencil/core/testing';
import { AppMoj } from '../app-moj';

describe('app-moj', () => {
  it('renders', async () => {
    const ts = new GlassLocaleService({});

    const page = await newSpecPage({
      components: [AppMoj],
      html: `<app-moj></app-moj>`,
    });

    const backRoute = "/countries/egov-moj";

    expect(page.root).toEqualHtml(`
      <app-moj>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="${backRoute}" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>title</ion-title>
            </ion-toolbar>
          </ion-header>

          <ion-content fullscreen="">
            <div class="d-content-section">
              
              <ion-item lines="none">
                <div>
                  <p>subtitle</p>
                  <h1>description</h1>
                </div>
              </ion-item>

              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="person-circle"></ion-icon>
                    requests.social.title
                  </ion-card-title>
                  <ion-card-subtitle>requests.social.subtitle</ion-card-subtitle>
                </ion-card-header>
                
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>requests.social.button</ion-label>
                  </ion-button>
               </div>
              </ion-card>   

              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="home"></ion-icon>
                    requests.sef.title
                  </ion-card-title>
                  <ion-card-subtitle>requests.sef.subtitle</ion-card-subtitle>
                </ion-card-header>
                
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>requests.sef.button</ion-label>
                  </ion-button>
               </div>
              </ion-card>   
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    <ion-icon name="pricetags"></ion-icon>
                    requests.seftemp.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    requests.seftemp.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <div class="d-card-footer">
                  <ion-button>
                    <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                    <ion-label>
                      requests.seftemp.button
                    </ion-label>
                  </ion-button>
                </div>
              </ion-card>
                
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-moj>
    `);
  });
});
