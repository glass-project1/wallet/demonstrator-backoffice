import { Component, h, Listen, State } from '@stencil/core';
import { getGlassSpinner, getGlassToast, GlassLocaleService, injectAndLocalize } from '@glass-project1/base-web-components';
import { inject } from '@glass-project1/db-decorators';;
import { error, info, stringFormat } from '@glass-project1/logging';
import { Native, RequestInfo, SEFBusinessManager } from '../../utils';
import { DIDResolver, GlassCodeRequest, RequestHandlerRegistry } from '@glass-project1/glass-toolkit';
import { FormResult } from '@glass-project1/ui-decorators-web';
import { ICountry, getCountry, getProxyRequestDisplayModal } from '../../utils/utils';

@Component({
  tag: 'app-moj-temp',
  styleUrl: 'app-moj-temp.scss',
  shadow: true,
})
export class AppMojTemp {

 /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
 @injectAndLocalize("sef.temp.issue", "GlassLocaleService")
 locale!: GlassLocaleService;
   
 @inject("RequestHandler")
 rh!: RequestHandlerRegistry;

 @inject("Native")
 native!: Native;
 
 @inject("SEFBusinessManager")
 manager!: SEFBusinessManager;
 
 @State()
 did?: string;

 @State()
 hasRequested: boolean = false;

 @State()
 requestErrorMessage?: string;

 @State()
 description: string = "";

 @State()
 country?: ICountry;

 private input: HTMLGlassInputElement;

 
 componentWillLoad() {
   this.description = this.locale.get("description") as string;

   if(!this.country)
     this.country = getCountry() as ICountry;

   this.description = stringFormat(this.description, !!this.country ? this.country.name : "");
 }

 async handleRequest(){
   const self: AppMojTemp = this;
   
   if (!self.did) {
     await getGlassToast().error('Inform the DID to proceed');
     return error.call(self, "No did");
   }

   const spinner = getGlassSpinner();
   // await spinner.show(stringFormat(self.locale.get("evidences.get.loading") as string, self.did));
   await spinner.show(stringFormat(self.locale.get("evidences.get.loading") as string, ""));
   
   self.manager.requestTempResidencePermitEvidences(self.did, this.country?.code, async (err: any, model?: any) => {
     if(spinner.isVisible())
       await spinner.remove();

     self.requestErrorMessage = "";
     let message = stringFormat(self.locale.get("evidences.get.success") as string, self.did as string);

     if (err) {
       err = typeof(err) === 'object' ?   JSON.stringify(err) : err;
       message = stringFormat(self.locale.get("evidences.get.error") as string, self.did as string) + err;
       self.requestErrorMessage = message;
       error.call(self, message);
     }
       
     this.hasRequested = true;

     return await getGlassToast()[(err ? 'error' : 'inform')](message)
     .then( _ => {
       info.call(self, self.locale.get("toast.success") as string + " - " + message)
     })
     .catch(e => {
       error.call(self, self.locale.get("toast.failure") as string)
     })

   })
 }

 
 private async handleDIDInput(did: string) {
   did = did.replace(/^\s+|\s+$|\s+(?=\s)/g, "");

   if(!did) {
     this.input.setValue("");
     return await getGlassToast().error('Inform the DID to proceed');
   }
    
   this.hasRequested = false;
   this.did = did;
 }

 private scanDid(){
   const self: AppMojTemp = this;
   self.rh.handleAsync(new GlassCodeRequest())
       .then(async (result: string) => {
         await self.input.setValue(result)
       })
       .catch(e => error.call(self, e))
 }

 private pasteDid(){
   const self: AppMojTemp = this;
   self.native.Clipboard.read()
       .then(async result => {
         await self.input.setValue(result.value)
       })
       .catch(e => error.call(self, e))
 }


 private getDIDForm(){
   const self: AppMojTemp = this;
   return (
     <ion-card>
       <ion-card-header>
         <ion-card-title>{this.locale.get("form.did.title")}</ion-card-title>
         <ion-card-subtitle>{this.locale.get("form.did.subtitle")}</ion-card-subtitle>
       </ion-card-header>
       
       <form-validate-submit 
         form-id="did-form"
         custom-validation="true"
         input-selector="ion-card-content glass-input"
         onSubmitEvent={(e: CustomEvent<FormResult>) => self.handleDIDInput(e.detail.result?.did)}>
         
           <div slot="fields">
             <ion-card-content>
               <glass-input 
                 input-id="did"
                 input-name="did"
                 label={self.locale.get("form.did.input.label") as string}
                 placeholder={self.locale.get("form.did.input.placeholder") as string}
                 value={self.did || ""}
                 required={true}
                 label-position="stacked"
                 has-end-slot={true}
                 lines="full"
                 ref={(e?: HTMLGlassInputElement) => self.input = e as HTMLGlassInputElement}
               >
               <ion-buttons slot="end">
                 <ion-button slot="secondary" onClick={self.pasteDid.bind(self)}>
                   <ion-icon slot="icon-only" name="clipboard"></ion-icon>
                 </ion-button>
                 <ion-button slot="primary" onClick={self.scanDid.bind(self)}>
                   <ion-icon slot="icon-only" name="qr-code"></ion-icon>
                 </ion-button>
               </ion-buttons>
             </glass-input>
           </ion-card-content>
         </div>
         
         <div slot="buttons">
           <div class="d-card-footer">
             <glass-button type="submit" color="primary" expand="block" fill="solid" label={this.locale.get("form.buttons.proceed") as string}></glass-button>
             <glass-button type="reset" color="gray0" expand="block" fill="solid" label={this.locale.get("form.buttons.clear") as string}></glass-button>
           </div>
         </div>
       </form-validate-submit>
     </ion-card>
   )
 }

 private getSendForEvidences(){
   const self: AppMojTemp = this;
   return (
     <ion-card>
       <ion-card-header>
         <ion-card-title>{this.locale.get("evidences.get.title")}</ion-card-title>
         <ion-card-subtitle>{this.locale.get("evidences.get.subtitle")}</ion-card-subtitle>
       </ion-card-header>
       <div class="d-card-description">
         {this.locale.get("evidences.get.description")}
       </div>
       <div class="d-card-footer">
         <glass-button type="submit" color="primary" expand="block" fill="solid" label={this.locale.get("form.buttons.submit") as string} onClick={self.handleRequest.bind(self)}></glass-button>
         <glass-button color="gray0" label={this.locale.get("form.buttons.back") as string} expand="block" fill="solid" onClick={() => {
           self.did = undefined;
         }}></glass-button>
       </div>
     </ion-card>
   )
 }

 private getRequestConclusion(){
   const self = this;
   return (
     <ion-card>
       <ion-card-header>
         <ion-card-title>{this.locale.get("evidences.get.title")}</ion-card-title>
         <ion-card-subtitle>{this.locale.get("evidences.get.subtitle")}</ion-card-subtitle>
       </ion-card-header>
       <div class={"d-card-conclusion " + (self.requestErrorMessage ? "error" : "")}>
         {self.requestErrorMessage ? self.requestErrorMessage : this.locale.get("evidences.get.conclusion")}
       </div>
       <div class="d-card-footer">
         <glass-button color="gray0" label={this.locale.get("form.buttons.back") as string} expand="block" fill="solid" onClick={() => {
           self.did = undefined;
         }}></glass-button>
       </div>
     </ion-card>
   )
 }

 private getContent(){
   if (!this.did)
     return this.getDIDForm();
     
   if (!this.hasRequested)
     return this.getSendForEvidences();
   
   return this.getRequestConclusion();
 }

 private getBackButton() {
   let backRoute = "/countries/egov-moj";

   if(!!this.country)
     backRoute = `/egov-moj/${this.country.name.toLowerCase()}`;

   return (
     <ion-back-button slot="start" defaultHref={backRoute} icon="chevron-back-outline"></ion-back-button>
   );
 }

 private getRequestInfo(country){
   switch(country.code) {
     case "pt":
       return RequestInfo.SEFTEMPPT;
     case "gr":
       return RequestInfo.SEFTEMPGR;
     case "tk":
       return RequestInfo.SEFTEMPTK;
     default:
       return RequestInfo.SEFTEMPPT;
   }
 }

 render() {
   const self: AppMojTemp = this;
   return [
     <ion-header collapse="fade" class="ion-no-border">
       <ion-toolbar color="primary">
         {this.getBackButton()}
         <ion-title>{this.locale.get("title")}</ion-title>
         <ion-buttons slot='end'>
           <ion-button class="g-button-proxy-request" fill='clear' title={self.locale.get("generate-request") as string} onClick={(evt) => getProxyRequestDisplayModal({
             data: self.getRequestInfo(self.country),
             header: self.locale.get("header") as string,
             label: self.locale.get("label") as string,
           })}>
             <ion-icon slot="icon-only" name="trail-sign"></ion-icon>
           </ion-button>
         </ion-buttons>
       </ion-toolbar>
     </ion-header>,
     <ion-content fullscreen={true}>
       <div class="d-content-section">
         <ion-item lines="none">
           <div>
             <p innerHTML={this.description}></p>
             <h1>{this.locale.get("subtitle")}</h1>
           </div>
         </ion-item>
         {this.getContent()}
       </div>
     </ion-content>
   ];
 }
}
