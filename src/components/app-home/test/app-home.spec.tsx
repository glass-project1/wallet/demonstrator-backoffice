import { newSpecPage } from '@stencil/core/testing';
import { AppHome } from '../app-home';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-home', () => {

  beforeAll(() => {
    const ls = new GlassLocaleService({});
  })

  it('renders', async () => {
  //   const page = await newSpecPage({
  //     components: [AppHome],
  //     html: `<app-home></app-home>`,
  //   });
  //   expect(page.root).toEqualHtml(`
  //     <app-home>
  //       <mock:shadow-root>
  //         <ion-content fullscreen="">
  //           <div class="d-background">
  //             <div class="d-welcome">
  //               <ion-item class="d-welcome-text" lines="none">
  //                 <ion-text>
  //                   <h1>
  //                     title
  //                   </h1>
  //                   <p>
  //                     subtitle
  //                   </p>
  //                 </ion-text>
  //               </ion-item>
  //               <ion-grid>
  //                 <ion-row>
  //                   <ion-col size="12">
  //                     <ion-card>
  //                       <ion-item lines="none">
  //                         <ion-icon name="person-circle"></ion-icon>
  //                         <ion-label>
  //                           <ion-text>
  //                             <h2>
  //                               egov.title
  //                             </h2>
  //                           </ion-text>
  //                         </ion-label>
  //                         <ion-button>
  //                           <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
  //                           <ion-label>
  //                             <ion-text>
  //                               egov.button
  //                             </ion-text>
  //                           </ion-label>
  //                         </ion-button>
  //                       </ion-item>
  //                       <ion-card-content>
  //                         egov.subtitle
  //                       </ion-card-content>
  //                     </ion-card>
  //                   </ion-col>
  //                   <ion-col size="12">
  //                     <ion-card>
  //                       <ion-item lines="none">
  //                         <ion-icon name="id-card"></ion-icon>
  //                         <ion-label>
  //                           <ion-text>
  //                             <h2>
  //                               egov-moj.title
  //                             </h2>
  //                           </ion-text>
  //                         </ion-label>
  //                         <ion-button>
  //                           <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
  //                           <ion-label>
  //                             <ion-text>
  //                               egov-moj.button
  //                             </ion-text>
  //                           </ion-label>
  //                         </ion-button>
  //                       </ion-item>
  //                       <ion-card-content>
  //                         egov-moj.subtitle
  //                       </ion-card-content>
  //                     </ion-card>
  //                   </ion-col>
  //                   <ion-col size="12">
  //                     <ion-card>
  //                       <ion-item lines="none">
  //                         <ion-icon name="card"></ion-icon>
  //                         <ion-label>
  //                           <ion-text>
  //                             <h2>
  //                               bank.title
  //                             </h2>
  //                           </ion-text>
  //                         </ion-label>
  //                         <ion-button>
  //                           <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
  //                           <ion-label>
  //                             <ion-text>
  //                               bank.button
  //                             </ion-text>
  //                           </ion-label>
  //                         </ion-button>
  //                       </ion-item>
  //                       <ion-card-content>
  //                         bank.subtitle
  //                       </ion-card-content>
  //                     </ion-card>
  //                   </ion-col>
  //                   <ion-col size="12">
  //                     <ion-card>
  //                       <ion-item lines="none">
  //                         <ion-icon name="hammer-outline"></ion-icon>
  //                         <ion-label>
  //                           <ion-text>
  //                             <h2>
  //                               pdmfc.title
  //                             </h2>
  //                           </ion-text>
  //                         </ion-label>
  //                         <ion-button>
  //                           <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
  //                           <ion-label>
  //                             <ion-text>
  //                               pdmfc.button
  //                             </ion-text>
  //                           </ion-label>
  //                         </ion-button>
  //                       </ion-item>
  //                       <ion-card-content>
  //                         pdmfc.subtitle
  //                       </ion-card-content>
  //                     </ion-card>
  //                   </ion-col>
  //                   <ion-col size="12">
  //                     <ion-card>
  //                       <ion-item lines="none">
  //                         <ion-icon name="home"></ion-icon>
  //                         <ion-label>
  //                           <ion-text>
  //                             <h2>
  //                               landlord.title
  //                             </h2>
  //                           </ion-text>
  //                         </ion-label>
  //                         <ion-button>
  //                           <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
  //                           <ion-label>
  //                             <ion-text>
  //                               landlord.button
  //                             </ion-text>
  //                           </ion-label>
  //                         </ion-button>
  //                       </ion-item>
  //                       <ion-card-content>
  //                         landlord.subtitle
  //                       </ion-card-content>
  //                     </ion-card>
  //                   </ion-col>
  //                 </ion-row>
  //               </ion-grid>
  //             </div>
  //           </div>
  //         </ion-content>
  //       </mock:shadow-root>
  //     </app-home>
  //   `);
  });
});
