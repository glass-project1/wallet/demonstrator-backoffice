import { GlassLocaleService, GlassRouter, injectAndLocalize } from '@glass-project1/base-web-components';
import { inject } from '@glass-project1/db-decorators';
import { Component, h } from '@stencil/core';
import { DemonstratorInfo, DEMONSTRATORS_LIST } from '../../utils';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.scss',
  shadow: true,
})
export class AppHome {

  /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
  @injectAndLocalize("home", "GlassLocaleService")
  locale!: GlassLocaleService;

  @inject("GlassRouter")
  router!: GlassRouter;

  private getButton(demonstrator: DemonstratorInfo){
    const self = this;

    return(
      <ion-button onClick={() => {self.router.push(demonstrator.route) }}>
        <ion-icon name='chevron-forward-outline' slot='end'></ion-icon>
        <ion-label>{this.locale.get(demonstrator.id + ".button")}</ion-label>
      </ion-button>
    )
  }

  generateDemonstratorCard(demonstrator: DemonstratorInfo): any{
    const self = this;

    return (
      <ion-card>
        <ion-card-header>
            <ion-card-title><ion-icon name={demonstrator.icon}></ion-icon>{this.locale.get(`${demonstrator.id}.title`)}</ion-card-title>
            <ion-card-subtitle>{this.locale.get(`${demonstrator.id}.subtitle`)}</ion-card-subtitle>
        </ion-card-header>
        <div class="d-card-footer">
          <ion-button size="small" onClick={() => { self.router.push(demonstrator.route) }}>
            <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
            {this.locale.get(`${demonstrator.id}.button`)}
          </ion-button>
       </div>
      </ion-card>   
    )
  }

  getContent(){
    const self = this;

    return DEMONSTRATORS_LIST.slice(0, DEMONSTRATORS_LIST.length).map((d) => self.generateDemonstratorCard(d));
  }

  render() {
    const self = this;

    return (
      <ion-content class="d-background" fullscreen={true}>
         <div class="d-content-section">
            <div>  
              <div>
                <h1>{this.locale.get("title")}</h1>
                <p>{this.locale.get("subtitle")}</p>
              </div>  
            </div>
            {self.getContent()}
        </div>
      </ion-content>
    );
  }

}