import { GlassLocaleService, injectAndLocalize } from '@glass-project1/base-web-components';
import { IEGovId } from '@glass-project1/glass-toolkit';
import { stringFormat } from '@glass-project1/logging';
import { Component, h, Listen, State, Watch } from '@stencil/core';
import { ICountry, getCountry } from '../../utils/utils';

@Component({
  tag: 'app-egov',
  styleUrl: 'app-egov.scss',
  shadow: true,
})
export class AppEgov {

  @injectAndLocalize("modg", "GlassLocaleService")
  locale!: GlassLocaleService;
  
  @State()
  subtitle: string = "";

  country?: ICountry;

  egovIdManagerName!: string;

  @State()
  loaded: boolean = false;

   /**
   * Keys to use for refresh infinite list
   */
   queryKeys: string[] | string = "id";

   /**
    * Stringified array with query for infinite list refresh
    */
   @State()
   private stringifiedQuery: string | string[] | undefined = undefined;
 
   /**
    * Value type in search bar input
    */
   @State()
   private typedQuery: string | undefined = undefined;

  
  componentWillLoad() {
    this.subtitle = this.locale.get("subtitle") as string;

    if(!this.country)
      this.country = getCountry() as ICountry;

    this.subtitle = stringFormat(this.subtitle, !!this.country ? this.country.name : "");

    this.egovIdManagerName = "EGovPtIdRestManager";

    switch (this.country?.code) {
      case "gr":
        this.egovIdManagerName = "EGovGrIdRestManager";
        break;
      case "tk":
        this.egovIdManagerName = "EGovTkIdRestManager";
    }

    this.loaded = true;  
  }
 
  handleSearchBarChange(event: CustomEvent) {
    return this.refreshListContent(!!event.detail ? event.detail : undefined);
  }

  refreshListContent(searchResult: {value: string, query: string} | undefined) {
    this.typedQuery = searchResult?.value || undefined;
    this.stringifiedQuery =  searchResult?.query || undefined;

    if(!!this.typedQuery)
      this.stringifiedQuery = JSON.stringify([`id=${this.typedQuery.trim()}&searching=true`]);
  }

  propsMapper(egovId: IEGovId) {
    return {
      "egov-id": egovId.id,
      "country": this.country?.code,
    };
  }
  
 
  private getList(){ 
    return (
      <glass-infinite-list hidden={!!this.country ? false : true}
        manager-name={this.egovIdManagerName}
        component-name="egov-list-item"
        element-mapper="egov-id"
        ref={e => (e as {})["elementMapper"] = this.propsMapper.bind(this)}
        query-limit={20}
        scroll-position="bottom"
        loading-text={this.locale.get("loading")}
        loading-spinner="bubbles"
        stringified-query={this.stringifiedQuery}
        typed-query={this.typedQuery}
        empty-query-title={this.locale.get("search.empty-title")}
        empty-query-subtitle={this.locale.get("search.empty-subtitle")}>
       
      </glass-infinite-list>
    )
  }

  private getLoadingList(){
    return (
      <div id="g-loading-container">
        <glass-spinner></glass-spinner>
      </div>
    );
  }

  getSearchBar() {
    return(
      <glass-searchbar
        query-keys={this.queryKeys}
        placeholder={this.locale.get("search.placeholder") as string}
        onSearchBarChangeEvent={this.handleSearchBarChange.bind(this)}>
      </glass-searchbar>
    );
  }

  getContent() {
    if(!this.loaded)
      return this.getLoadingList();

    return this.getList();
  }
  render() {
    return [
      <ion-header collapse="fade" class="ion-no-border">  
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/countries/egov" icon="chevron-back-outline"></ion-back-button>
          </ion-buttons>
          <ion-title>{this.locale.get("title")}</ion-title>
          <ion-buttons slot="end">
            <glass-searchbar-toggle-button></glass-searchbar-toggle-button>
          </ion-buttons>
        </ion-toolbar>
        {this.getSearchBar()}
      </ion-header>,
      <ion-content>
        <div class="d-content-section">
          <ion-item lines="none">
            <div>
              <p>{this.subtitle}</p>
              <h1>{this.locale.get("list.title")}</h1>
              <h3>{this.locale.get("list.description")}</h3>
            </div>
          </ion-item>
          {this.getContent()}
        </div>
      </ion-content>
    ]
  }

}