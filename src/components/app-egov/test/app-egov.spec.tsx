import { newSpecPage } from '@stencil/core/testing';
import { AppEgov } from '../app-egov';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-egov', () => {

  beforeAll(() => {
    const ls = new GlassLocaleService({});
  })

  it('renders', async () => {
    const page = await newSpecPage({
      components: [AppEgov],
      html: `<app-egov></app-egov>`,
    });
    
    expect(page.root).toEqualHtml(`
      <app-egov>
        <mock:shadow-root>
          <ion-header collapse="fade" class="ion-no-border">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaultHref="/countries/egov" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>title</ion-title>
              <ion-buttons slot="end">
                <glass-searchbar-toggle-button></glass-searchbar-toggle-button>
              </ion-buttons>
            </ion-toolbar>
            <glass-searchbar placeholder="search.placeholder" query-keys="id"></glass-searchbar>
          </ion-header>
          <ion-content>
            <div class="d-content-section">
              <ion-item lines="none">
                <div>
                  <p>subtitle</p>
                  <h1>list.title</h1>
                  <h3>list.description</h3>
                </div>
              </ion-item>
              <glass-infinite-list component-name="egov-list-item" element-mapper="egov-id" empty-query-subtitle="search.empty-subtitle" empty-query-title="search.empty-title" hidden="" loading-spinner="bubbles" loading-text="loading" manager-name="EGovPtIdRestManager" query-limit="20" scroll-position="bottom"></glass-infinite-list>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-egov>
    `);
  });
});
