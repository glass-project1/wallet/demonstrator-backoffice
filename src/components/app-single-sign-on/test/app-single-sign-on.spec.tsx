import { newSpecPage } from '@stencil/core/testing';
import { AppSingleSignOn } from '../app-single-sign-on';
import {GlassLocaleService} from "@glass-project1/base-web-components";

describe('app-single-sign-on', () => {

  beforeAll(() => {
    const ts = new GlassLocaleService({})
  })

  it('renders', async () => {
    const page = await newSpecPage({
      components: [AppSingleSignOn],
      html: `<app-single-sign-on></app-single-sign-on>`,
    });
    expect(page.root).toEqualHtml(`
      <app-single-sign-on>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="/" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>
                title
              </ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content fullscreen="">
            <div class="d-content-section">
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    title
                  </ion-card-title>
                  <ion-card-subtitle>
                    description
                  </ion-card-subtitle>
                </ion-card-header>
                <ion-card-content>
                  <form-validate-submit custom-validation="true" force-report="" form-id="sign-on-form" input-selector="glass-input, glass-crud-input">
                    <div slot="fields">
                      <glass-input input-id="namespace" input-name="namespace" interface="action-sheet" label="input.namespace.label" options-stringify="{&quot;egov.pt&quot;:{&quot;label&quot;:&quot;eGovernance Portugal&quot;,&quot;value&quot;:&quot;egov.pt&quot;},&quot;egov.gr&quot;:{&quot;label&quot;:&quot;eGovernance Greece&quot;,&quot;value&quot;:&quot;egov.gr&quot;},&quot;egov.tk&quot;:{&quot;label&quot;:&quot;eGovernance Turkey&quot;,&quot;value&quot;:&quot;egov.tk&quot;}}" required="" type="options" value="egov.pt"></glass-input>
                      <glass-input input-id="requester" input-name="requester" label="input.requester.label" minlength="5" placeholder="input.requester.placeholder" required=""></glass-input>
                      <glass-input input-id="purpose" input-name="purpose" label="input.purpose.label" minlength="10" placeholder="input.purpose.placeholder" required=""></glass-input>
                      <glass-input disabled="" has-end-slot="" input-id="redirect" input-name="redirect" label="input.redirect.label" required="" value="http://testing.stenciljs.com/signon.html?reply=">                        <div slot="end" title="input.redirect.information">
                          <ion-icon name="information-circle-outline"></ion-icon>
                        </div>
                      </glass-input>
                    </div>
                    <div slot="buttons">
                      <div class="d-card-footer">
                        <glass-button color="primary" expand="block" fill="solid" type="submit" label="buttons.proceed"></glass-button>
                        <glass-button type="reset" expand="block" color="gray0" href="/" label="buttons.back"></glass-button>
                      </div>
                    </div>
                  </form-validate-submit>
                </ion-card-content>
              </ion-card>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-single-sign-on>
    `);
  });
});
