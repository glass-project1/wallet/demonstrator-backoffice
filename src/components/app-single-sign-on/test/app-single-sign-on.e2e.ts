import { newE2EPage } from '@stencil/core/testing';

describe.skip('app-single-sign-on', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<app-single-sign-on></app-single-sign-on>');

    const element = await page.find('app-single-sign-on');
    expect(element).toHaveClass('hydrated');
  });
});
