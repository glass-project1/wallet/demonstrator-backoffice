import {Component, h, State} from '@stencil/core';
import {GlassLocaleService, injectAndLocalize} from "@glass-project1/base-web-components";
import {FormResult} from "@glass-project1/ui-decorators-web";
import {debug, error} from '@glass-project1/logging';
import {getCrypoApi} from "@glass-project1/opendsu-types";
import {stringFormat} from "@glass-project1/decorator-validation";
import {CHALLENGES} from "@glass-project1/glass-toolkit";

const SUPPORTED_SIGNONS = {
  "egov.pt": {
    label: "eGovernance Portugal",
    value: "egov.pt"
  },
  "egov.gr": {
    label: "eGovernance Greece",
    value: "egov.gr"
  },
  "egov.tk": {
    label: "eGovernance Turkey",
    value: "egov.tk"
  }
}


@Component({
  tag: 'app-single-sign-on',
  styleUrl: 'app-single-sign-on.scss',
  shadow: true,
})
export class AppSingleSignOn {

  @injectAndLocalize("sign-on", "GlassLocaleService")
  locale!: GlassLocaleService;

  @State()
  request?: {} = undefined;


  private calculateEndpoint(){
    const {hostname, protocol, port} = window.location;
    return `${protocol}//${hostname}${port ? `:${port}` : ""}`
  }

  private getRedirect(){
    return `${this.calculateEndpoint()}/signon.html?reply=`
  }


  private onSubmit(e: CustomEvent<FormResult>){
    const {action, result} = e.detail;
    if (action !== "submit")
      return debug.call(this, "invalid submit")

    this.request = Object.assign({}, result, {
      challengeType: CHALLENGES.SIGNED_TOKEN
    });
  }

  private getContent(){
    const self = this;
    if (self.request)
      return (
          <glass-timer duration={5}
                       description={stringFormat(self.locale.get("requesting") as string, JSON.stringify(self.request, undefined, 2))}
                       onTimerExpireEvent={() => {
                         let payload;

                         try {
                           payload = getCrypoApi().encodeBase58(JSON.stringify((self.request as any)));
                         } catch (e: any) {
                           return error.call(this, e);
                         }

                         const url = `${this.calculateEndpoint()}/signon.html?request=${payload}`;
                         window.location.replace(url);
                       }}
                       ref={async (e?: HTMLGlassTimerElement) => {
                         if (!e)
                           return;
                         await e.start()
                       }}
          ></glass-timer>
      )

    return (
        <form-validate-submit onSubmitEvent={self.onSubmit.bind(self)}
                              force-report={true}
                              custom-validation="true"
                              form-id="sign-on-form"
                              input-selector="glass-input, glass-crud-input">
          <div slot="fields">
            <glass-input type="options"
                         required={true}
                         input-id="namespace"
                         input-name="namespace"
                         label={self.locale.get("input.namespace.label") as string}
                         options-stringify={JSON.stringify(SUPPORTED_SIGNONS)}
                         value={SUPPORTED_SIGNONS["egov.pt"].value}
                         interface="action-sheet"
            ></glass-input>
            <glass-input required={true}
                         minlength={5}
                         input-id="requester"
                         input-name="requester"
                         label={self.locale.get("input.requester.label") as string}
                         placeholder={self.locale.get("input.requester.placeholder") as string}
            ></glass-input>
            <glass-input required={true}
                         minlength={10}
                         input-id="purpose"
                         input-name="purpose"
                         label={self.locale.get("input.purpose.label") as string}
                         placeholder={self.locale.get("input.purpose.placeholder") as string}
            ></glass-input>
            <glass-input required={true}
                         disabled={true}
                         input-id="redirect"
                         input-name="redirect"
                         label={self.locale.get("input.redirect.label") as string}
                         value={self.getRedirect()}
                         has-end-slot={true}
            >
              <div slot="end" title={self.locale.get("input.redirect.information") as string}>
                <ion-icon name="information-circle-outline"></ion-icon>
              </div>
            </glass-input>
          </div>
          <div slot="buttons">
            <div class="d-card-footer">
              <glass-button type="submit" fill="solid" color="primary" expand="block" label={self.locale.get("buttons.proceed") as string}></glass-button>
              <glass-button type="reset" expand="block" color="gray0" href="/"  label={self.locale.get("buttons.back") as string}></glass-button>
            </div>
          </div>
        </form-validate-submit>
    )
  }

  render() {
    const self = this;

    return [
      <ion-header collapse="fade" class="ion-no-border">
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
          </ion-buttons>
          <ion-title>{this.locale.get("title")}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <div class="d-content-section">
          <ion-card>
            <ion-card-header>
              <ion-card-title>{this.locale.get("title")}</ion-card-title>
              <ion-card-subtitle>{this.locale.get("description")}</ion-card-subtitle>
            </ion-card-header>
            <ion-card-content>
              {self.getContent()}
            </ion-card-content>
          </ion-card>
        </div>
      </ion-content>
    ];
  }

}
