import { GlassLocaleService, GlassRouter, injectAndLocalize } from '@glass-project1/base-web-components';
import { inject } from '@glass-project1/db-decorators';
import { Component, h } from '@stencil/core';
import { DemonstratorInfo } from '../../utils';
import { DISABILITY_REPORT_OPTIONS } from '../../utils/DisabilityReportBusinessManager';
import { stringFormat } from '@glass-project1/logging';
import { count } from 'console';

@Component({
  tag: 'app-disability',
  styleUrl: 'app-disability.scss',
  shadow: true,
})
export class AppDisability {
  
  /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
  @injectAndLocalize("disability", "GlassLocaleService")
  locale!: GlassLocaleService;

  @inject("GlassRouter")
  router!: GlassRouter;

  handleSelectCountry(demonstrator:{[indexer: string]: any}, country: {[indexer: string]: any}) {
    const self: AppDisability = this;
    self.router.push(`${demonstrator.route}/${country.name.toLowerCase()}`) 
  }

  private getButton(demonstrator: DemonstratorInfo, country: {[index: string]: any}){
    const self: AppDisability = this;
    
    return(
      <ion-button onClick={() => self.handleSelectCountry(demonstrator, country)}>
        <ion-icon name='chevron-forward-outline' slot='end'></ion-icon>
        <ion-label>{this.locale.get("requests." + demonstrator.id + ".button")}</ion-label>
      </ion-button>
    );

  }

  generateDemonstratorCard(demonstrator: DemonstratorInfo): any{
    const self: AppDisability = this;
    const title = this.locale.get(`requests.${demonstrator.id}.title`) as string;
    const countries = [{name: "Portugal", code: "pt"}, {name:"Greece",  code: "gr"}, {name:"Turkey",  code: "tk"}];

    return (
      <div> 
        {countries.map(country => {
          return (
            <ion-card>
              <ion-card-header>
                  <ion-card-title><ion-icon name={demonstrator.icon}></ion-icon>{stringFormat(title, country.name)}</ion-card-title>
                  <ion-card-subtitle>{this.locale.get(`requests.${demonstrator.id}.subtitle`)}</ion-card-subtitle>
              </ion-card-header>
              <div class="d-card-footer">
                {self.getButton(demonstrator, country)}
              </div>
            </ion-card>
          )
        })} 
      </div>
    );

  }

  getContent(){
    const self = this;

    return DISABILITY_REPORT_OPTIONS.slice(0, DISABILITY_REPORT_OPTIONS.length).map((d) => self.generateDemonstratorCard(d));
  }

  render() {
    const self = this;

    return [
      <ion-header collapse="fade" class="ion-no-border">
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
          </ion-buttons>
          <ion-title>{this.locale.get("title")}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <div class="d-content-section">
          <ion-item lines="none">
            <div>
              <p>{this.locale.get("subtitle")}</p>
              <h1>{this.locale.get("title")}</h1>
            </div>
          </ion-item>
          
          {self.getContent()}
        </div>
          
      </ion-content>
    ];
  }

}

