import { GlassLocaleService, GlassRouter, injectAndLocalize } from '@glass-project1/base-web-components';
import { inject } from '@glass-project1/db-decorators';
import { stringFormat } from '@glass-project1/logging';
import { Component, h } from '@stencil/core';


interface Country {
  name: string;
  code: string;
};

@Component({
  tag: 'app-egov-countries',
  styleUrl: 'app-egov-countries.scss',
  shadow: true,
})
export class AppEgovCountries {

  @injectAndLocalize("countries", "GlassLocaleService")
  locale!: GlassLocaleService;

  @inject("GlassRouter")
  router!: GlassRouter;
  
  items: Country[] = [{name: "Portugal", code: "pt"}, {name:"Greece",  code: "gr"}, {name:"Turkey",  code: "tk"}];

  private targetComponentName?: string;

  componentWillLoad(){
    let {pathname} = window.location;
    this.targetComponentName = pathname.split("/").pop();
  }

  handleSelectCountry(country: Country) {
    const self: AppEgovCountries = this;
    self.router.push(`${this.targetComponentName}/${country.name.toLowerCase()}`)
  }
  
  getItems() {
    const self: AppEgovCountries = this;
    const title = self.locale.get(`${this.targetComponentName}.description`) as string;

    return (
      <div> 
        {self.items.map(country => {
          return (
            <ion-card>
              <ion-card-header>
                  <ion-card-title><ion-icon name="location-outline"></ion-icon>{stringFormat(title, country.name)}</ion-card-title>
              </ion-card-header>
              <div class="d-card-footer">
                <ion-button onClick={() => self.handleSelectCountry(country)}>
                  <ion-icon name='chevron-forward-outline' slot='end'></ion-icon>
                  <ion-label>{self.locale.get("button")}</ion-label>
                </ion-button>
              </div>
            </ion-card>
          )
        })} 
      </div>
    );
  }
  render() {
    return [
      <ion-header collapse="fade" class="ion-no-border">
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
             <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
          </ion-buttons>
          <ion-title>{this.locale.get(`${this.targetComponentName}.title`)}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <div class="d-content-section">
          <ion-item lines="none">
            <div>
              <p>{this.locale.get(`${this.targetComponentName}.subtitle`)}</p>
              <h1>{this.locale.get(`${this.targetComponentName}.title`)}</h1>
            </div>
          </ion-item>
          {this.getItems()}
        </div>
      </ion-content>
    ]
  }

}
