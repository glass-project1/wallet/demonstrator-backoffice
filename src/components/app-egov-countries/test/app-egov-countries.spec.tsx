import { newSpecPage } from '@stencil/core/testing';
import { AppEgovCountries } from '../app-egov-countries';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-egov-countries', () => {
  const ts = new GlassLocaleService({});

  it('renders', async () => {
    const page = await newSpecPage({
      components: [AppEgovCountries],
      html: `<app-egov-countries></app-egov-countries>`,
    });
    const config = {
      backroute: "/",
      icon: "location-outline",
      locale: "countries"
    };
    
    expect(page.root).toEqualHtml(`
      <app-egov-countries>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>.title</ion-title>
            </ion-toolbar>
          </ion-header>

          <ion-content fullscreen="">
            <div class="d-content-section">
              
              <ion-item lines="none">
                <div>
                  <p>.subtitle</p>
                  <h1>.title</h1>
                </div>
              </ion-item>

              <div>
                <ion-card>
                  <ion-card-header>
                    <ion-card-title>
                      <ion-icon name=${config.icon}></ion-icon>
                      .description
                    </ion-card-title>
                  </ion-card-header>
                  
                  <div class="d-card-footer">
                    <ion-button>
                      <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                      <ion-label>button</ion-label>
                    </ion-button>
                  </div>
                </ion-card>   
             
                <ion-card>
                  <ion-card-header>
                    <ion-card-title>
                      <ion-icon name=${config.icon}></ion-icon>
                      .description
                    </ion-card-title>
                  </ion-card-header>
                  
                  <div class="d-card-footer">
                    <ion-button>
                      <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                      <ion-label>button</ion-label>
                    </ion-button>
                  </div>
                </ion-card>   
              
                <ion-card>
                  <ion-card-header>
                    <ion-card-title>
                      <ion-icon name=${config.icon}></ion-icon>
                      .description
                    </ion-card-title>
                  </ion-card-header>
                  
                  <div class="d-card-footer">
                    <ion-button>
                      <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
                      <ion-label>button</ion-label>
                    </ion-button>
                  </div>
                </ion-card>   
              </div>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-egov-countries>
    `);
  });
});
