import { newSpecPage } from '@stencil/core/testing';
import { EgovListItem } from '../egov-list-item';

describe('egov-list-item', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [EgovListItem],
      html: `<egov-list-item></egov-list-item>`,
    });
    expect(page.root).toEqualHtml(`
      <egov-list-item>
        <mock:shadow-root>
          <div class="d-item">
            <ion-item>
              <ion-thumbnail slot="start">
                <ion-skeleton-text></ion-skeleton-text>
              </ion-thumbnail>
              <h3>
                <ion-skeleton-text animated="" style="width: 60%;"></ion-skeleton-text>
              </h3>
              <h4>
                <ion-skeleton-text animated="" style="width: 100%;"></ion-skeleton-text>
              </h4>
            </ion-item>
          </div>
        </mock:shadow-root>
      </egov-list-item>
    `);
  });
});
