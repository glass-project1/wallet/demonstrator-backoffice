import { BarCodeType, getGlassPopover, getGlassSpinner, getGlassToast, GlassLocaleService, injectAndLocalize, Spinner } from "@glass-project1/base-web-components";
import { getInjectablesRegistry, inject } from "@glass-project1/db-decorators";
import { all, Err, error, errorCallback, debug, info, stringFormat } from "@glass-project1/logging";
import { Component, h, Method, Prop, State, Watch } from "@stencil/core";
import { EGovPtAppRestManager, EGovEntity, EGovGrAppRestManager, EGovGrIdRestManager, EGovPtIdRestManager, EGovTkAppRestManager, EGovTkIdRestManager, IMG_SOURCE} from "../../utils";
import {IEGovId} from "@glass-project1/glass-toolkit";
import {EGovId} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovId";
import { getProxyRequestDisplayModal } from "../../utils/utils";


@Component({
  tag: "egov-list-item",
  styleUrl: "egov-list-item.scss",
  shadow: true,
})
export class EgovListItem {

  @Prop({attribute: "egov-id", mutable: true}) egovId: string;
  @Prop({attribute: "country", mutable: true}) country: string;


  /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
  @injectAndLocalize("egov.install", "GlassLocaleService")
  locale!: GlassLocaleService;
  
  managerEGovID: EGovPtIdRestManager | EGovGrIdRestManager | EGovTkIdRestManager;
  managerEGov: EGovPtAppRestManager | EGovGrAppRestManager | EGovTkAppRestManager;

  @State()
  identity?: IEGovId = undefined;

  @State()
  data?: string = undefined;

  componentWillLoad() {
    const self = this;

    switch (self.country) {
      case "gr":
        self.managerEGov = getInjectablesRegistry().get("EGovGrAppRestManager") as EGovGrAppRestManager;
        self.managerEGovID = getInjectablesRegistry().get("EGovGrIdRestManager") as EGovGrIdRestManager;
        break;
      case "tk":
        self.managerEGov = getInjectablesRegistry().get("EGovTkAppRestManager") as EGovTkAppRestManager;
        self.managerEGovID = getInjectablesRegistry().get("EGovTkIdRestManager") as EGovTkIdRestManager;
        break;
      default:
        self.managerEGov = getInjectablesRegistry().get("EGovPtAppRestManager") as EGovPtAppRestManager;
        self.managerEGovID = getInjectablesRegistry().get("EGovPtIdRestManager") as EGovPtIdRestManager;
    }    
  }
  
  componentDidLoad(){
    const self: EgovListItem = this;

    self.refresh(self.egovId)
      .then( _ => all.call(self, "Identity updated"))
      .catch( e => error.call(self, e));
  }

  
  @Watch("egovId")
  @Method()
  async refresh(newValue?: string) {
    const self: EgovListItem = this;

    const idString: string = newValue || this.egovId;

    return new Promise<void>((resolve, reject) => {
      if(!idString)
        return errorCallback.call(self, "No id defined", reject, idString);


      self.managerEGovID.read(idString, (err: Err, identity?: IEGovId) => {

        self.data = undefined;
        
        if (err || !identity)
          return errorCallback.call(self, err || "identity for id {0} not found!", reject, idString);

        self.identity = identity;

        if(self.egovId !== idString)
          self.egovId = idString;

        debug.call(self, "identity with id {0} loaded", this.egovId);
        self.managerEGov.read(idString, (err: Err, installRequest?: EGovEntity) => {
          if (err || !installRequest)
            return resolve()

          self.data = self.fixEGovDappString(installRequest.walletKeySSI as string)
          resolve();
        })  
      })
    })
  }

  private getAvatar(imgSrc: string){
    if(!imgSrc)
      return;

    return(
      <ion-avatar>
        <img src={`../${imgSrc}`} alt="identity"/>
      </ion-avatar>
    )
  }

  private getImgSource(identity: IEGovId){
    const self: EgovListItem = this;

    return self.identity?.gender === "Male" ? IMG_SOURCE.MP : IMG_SOURCE.FP;
  }

  private fixEGovDappString(payload: string): string{
    return payload.replace("\\\"", "\"");
  }

  private async createEgovDApp(event: Event){
    const self: EgovListItem = this;
    const target = (event.currentTarget as HTMLButtonElement);
    const targetIcon = target.innerHTML;
    
    target.innerHTML = '<ion-spinner name="circular"></ion-spinner>';

    return new Promise<void>(async (resolve, reject) => {
      self.managerEGov.create(self.egovId, {}, async  (err: Err, result?: EGovEntity) => {
        if(err || !result){
          // await self.spinner.remove();
          await getGlassToast().error(stringFormat("Unable to create EGov with id {0}", self.egovId));
          target.innerHTML = targetIcon;
          return errorCallback.call(self, err || "Unable to create EGov with id {0}", reject, self.egovId);
        }
        self.data = self.fixEGovDappString(result.walletKeySSI as string);
        info.call(self, "Install EGov String created: " + self.data)
        await getGlassToast().inform(stringFormat("EGov with id {0} successfully installed", self.egovId));
        target.innerHTML = '<ion-icon size="large" name="qr-code"></ion-icon>';
        // await self.refresh(self.egovId);
        resolve();
      })
    })
  }

  async presentPopOver(e: Event){
    const self = this;
    info.call(self,  "Install EGov String: " + self.data);

    getProxyRequestDisplayModal({
      data: self.data as string,
      header: self.locale.get("header") as string + self.identity?.givenName,
      label: self.locale.get("label") as string,
    })
  }


  private async presentLockToast(event: Event) {
    const self: EgovListItem = this;
    const toast = await getGlassToast().error(stringFormat("Egov ID {0} is locked by another installation proccess", (self.identity?.id as string)));

    await self.refresh(self.identity?.id)
  }

  private getButton(){
    const self: EgovListItem = this;

    if(!!self.identity?.locked)
      return(
        <ion-button size="large" onClick={(evt) => self.presentLockToast(evt)}>
          <ion-icon size="large" name="lock-closed-outline"></ion-icon>
        </ion-button>
      )

    if(!!self.data) 
      return(
        <ion-button size="large" onClick={(evt: any) => self.presentPopOver(evt)}>
          <ion-icon size="large" name="qr-code"></ion-icon>
        </ion-button>
      )
 
    return (
      <ion-button class="d-installed" size="large" onClick={ async (event) => { await self.createEgovDApp(event) }}>
        <ion-icon size="large" name="download"></ion-icon>
      </ion-button>
    )
  }

  private getLoaded(){
    const self: EgovListItem = this;

    return [
      <div class="d-icon">
        {self.getAvatar(self.getImgSource(self.identity as IEGovId))}
      </div>,
      <ion-label>
        <h3 class="d-identity-name">
          {self.identity?.givenName}
        </h3>
        <h4 class="d-identity-number">{self.identity?.id}</h4>
      </ion-label>,
      self.getButton()
    ]
  }

  private getLoading(){
    return [
      <ion-thumbnail slot="start">
        <ion-skeleton-text></ion-skeleton-text>
      </ion-thumbnail>,
      <h3>
        <ion-skeleton-text animated style={{width: "60%"}}></ion-skeleton-text>
      </h3>,
      <h4>
        <ion-skeleton-text animated style={{width: "100%"}}></ion-skeleton-text>
      </h4>
    ]
  }

  private getContent(){
    const self: EgovListItem = this;

    return(
      <div class="d-item">
        <ion-item>
          {self.identity ? self.getLoaded() : self.getLoading()}
        </ion-item>
      </div>
    )
  }

  render() {
    const self: EgovListItem = this;

    return(
      self.getContent()
    )
  }

}