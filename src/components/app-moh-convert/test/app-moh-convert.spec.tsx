import { newSpecPage } from '@stencil/core/testing';
import { AppMohConvert } from '../app-moh-convert';
import { GlassLocaleService } from '@glass-project1/base-web-components';

describe('app-moh-convert', () => {
  it('renders', async () => {
    const ts = new GlassLocaleService({});
    const page = await newSpecPage({
      components: [AppMohConvert],
      html: `<app-moh-convert></app-moh-convert>`,
    });
    expect(page.root).toEqualHtml(`
      <app-moh-convert>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-back-button defaulthref="/countries/egov-moh" icon="chevron-back-outline" slot="start"></ion-back-button>
              <ion-title>
                title
              </ion-title>
              <ion-buttons slot="end">
                <ion-button class="g-button-proxy-request" fill="clear" title="generate-request">
                  <ion-icon name="trail-sign" slot="icon-only"></ion-icon>
                </ion-button>
              </ion-buttons>
            </ion-toolbar>
          </ion-header>
          <ion-content fullscreen="">
            <div class="d-content-section">
              <ion-item lines="none">
                <div>
                  <p>
                    description
                  </p>
                  <h1>
                    subtitle
                  </h1>
                </div>
              </ion-item>
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    form.did.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    form.did.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <form-validate-submit custom-validation="true" form-id="did-form" input-selector="ion-card-content glass-input">
                  <div slot="fields">
                    <ion-card-content>
                      <glass-input has-end-slot="" input-id="did" input-name="did" label="form.did.input.label" label-position="stacked" lines="full" placeholder="form.did.input.placeholder" required="" value="">
                        <ion-buttons slot="end">
                          <ion-button slot="secondary">
                            <ion-icon name="clipboard" slot="icon-only"></ion-icon>
                          </ion-button>
                          <ion-button slot="primary">
                            <ion-icon name="qr-code" slot="icon-only"></ion-icon>
                          </ion-button>
                        </ion-buttons>
                      </glass-input>
                    </ion-card-content>
                  </div>
                  <div slot="buttons">
                    <div class="d-card-footer">
                      <glass-button color="primary" expand="block" fill="solid" label="form.buttons.proceed" type="submit"></glass-button>
                      <glass-button color="gray0" expand="block" fill="solid" label="form.buttons.clear" type="reset"></glass-button>
                    </div>
                  </div>
                </form-validate-submit>
              </ion-card>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-moh-convert>
    `);
  });
});
