import { GlassLocaleService, GlassRouter, injectAndLocalize } from '@glass-project1/base-web-components';
import { inject } from '@glass-project1/db-decorators';
import { Component, h } from '@stencil/core';
import { DemonstratorInfo, LANDLORD_OPTIONS } from '../../utils';


@Component({
  tag: 'app-landlord',
  styleUrl: 'app-landlord.scss',
  shadow: true,
})
export class AppLandlord {

  /**
   * the {@link inject}ed {@link GlassLocaleService}
   */
  @injectAndLocalize("landlord", "GlassLocaleService")
  locale!: GlassLocaleService;

  @inject("GlassRouter")
  router!: GlassRouter;

  private getButton(demonstrator: DemonstratorInfo){
    const self = this;

    return(
      <ion-button onClick={() => { self.router.push(demonstrator.route); }}>
        <ion-icon name="chevron-forward-outline" slot="end"></ion-icon>
        <ion-label>{this.locale.get(`requests.${demonstrator.id}.button`)}</ion-label>
      </ion-button>
    )

  }

  generateDemonstratorCard(demonstrator: DemonstratorInfo) {
    return (
      <ion-card>
        <ion-card-header>
          <ion-card-title>
            <ion-icon name={demonstrator.icon}></ion-icon>
            {this.locale.get(`requests.${demonstrator.id}.title`)}
          </ion-card-title>
          <ion-card-subtitle>{this.locale.get(`requests.${demonstrator.id}.subtitle`)}</ion-card-subtitle>
        </ion-card-header>
        <div class="d-card-footer">
          {this.getButton(demonstrator)}
       </div>
      </ion-card>   
    );
  }

  getContent(){
    const self = this;
    return LANDLORD_OPTIONS.slice(0, LANDLORD_OPTIONS.length).map((d) => self.generateDemonstratorCard(d));
  }

  render() {
    return [
      <ion-header collapse="fade" class="ion-no-border">
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/" icon="chevron-back-outline"></ion-back-button>
          </ion-buttons>
          <ion-title>{this.locale.get("title")}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <div class="d-content-section">
          <ion-item lines="none">
            <div>
              <p>{this.locale.get("subtitle")}</p>
              <h1>{this.locale.get("title")}</h1>
            </div>
          </ion-item>
          {this.getContent()}
        </div>
      </ion-content>,
    ];
  }

}
