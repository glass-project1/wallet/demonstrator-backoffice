import { inject } from '@glass-project1/db-decorators';
import { Component, Host, Prop, h } from '@stencil/core';
import { Native } from '../../utils/native';
import { getGlassToast } from '@glass-project1/base-web-components';

@Component({
  tag: 'glass-proxy-request-display',
  styleUrl: 'glass-proxy-request-display.scss',
  shadow: true,
})
export class GlassProxyRequestDisplay {
  @inject("Native")
  native!: Native;

  @Prop({attribute: "data"})
  data?: string;

  @Prop({attribute: "header-title"})
  header?: string;

  @Prop({attribute: "label-title"})
  label?: string;

  @Prop({attribute: "modal"})
  modal?: HTMLIonModalElement;

  private getQRCode(){
    const self = this;

    return (
      <div>
        <div class="g-code-container">
          <div>
            <div class="g-code-generator">
              <glass-code-generator data={self.data}></glass-code-generator>
            </div>
          </div>
        </div>
        <div class="g-did-container">
          <glass-input lines="none" labelPosition="stacked" readonly={true} label={self.label} value={self.data}></glass-input>
          <ion-button onClick={self.copyData.bind(self)}>
            <ion-icon name='clipboard'></ion-icon>
          </ion-button>
        </div>
      </div>
    )
  }

  private copyData() {
    const self = this;
    self.native.Clipboard.write( {string: self.data as string})
    .then(() => { 
      getGlassToast().inform("Data was successfully copied")

    })
    .catch((e) => getGlassToast().error("Failed to copy data"));
  }


  render() {
    const self: GlassProxyRequestDisplay = this;
    return [
      <ion-header collapse="fade" class="ion-no-border">
      <ion-toolbar color="primary">
        <ion-buttons slot="start">
          <ion-button onClick={() => self.modal?.dismiss()}>
              <ion-icon slot="icon-only" name="chevron-back-outline"></ion-icon>
          </ion-button>
        </ion-buttons>
        <ion-title>{self.header}</ion-title>
        <ion-buttons slot="end">
          <ion-button onClick={() => self.modal?.dismiss()}>
            <ion-icon slot="icon-only" name="close-outline"></ion-icon>
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>,
      <ion-content>
        <div class="g-content-section">
          <ion-card>
            <ion-card-content>
              {self.getQRCode()}
            </ion-card-content>
          </ion-card>
        </div>

      </ion-content>
    ];
  }

}
