import { GlassLocaleService } from "@glass-project1/base-web-components";
import { newSpecPage } from "@stencil/core/testing";
import { AppPdmfcWorkContract } from "../app-pdmfc-work-contract";

describe("app-pdmfc-work-contract", () => {
  beforeAll(() => {
    const ls = new GlassLocaleService({});
  });

  it("renders", async () => {
    const page = await newSpecPage({
      components: [AppPdmfcWorkContract],
      html: `<app-pdmfc-work-contract></app-pdmfc-work-contract>`,
    });
    const backRoute = "/pdmfc";

    expect(page.root).toEqualHtml(`
      <app-pdmfc-work-contract>
        <mock:shadow-root>
          <ion-header class="ion-no-border" collapse="fade">
            <ion-toolbar color="primary">
              <ion-buttons slot="start">
                <ion-back-button defaulthref="/pdmfc" icon="chevron-back-outline"></ion-back-button>
              </ion-buttons>
              <ion-title>
                title
              </ion-title>
              <ion-buttons slot="end">
                <ion-button class="g-button-proxy-request" fill="clear" title="generate-request">
                  <ion-icon name="trail-sign" slot="icon-only"></ion-icon>
                </ion-button>
              </ion-buttons>
            </ion-toolbar>
          </ion-header>
          <ion-content fullscreen="">
            <div class="d-content-section">
              <ion-item lines="none">
                <div>
                  <p>
                    description
                  </p>
                  <h1>
                    subtitle
                  </h1>
                </div>
              </ion-item>
              <ion-card>
                <ion-card-header>
                  <ion-card-title>
                    form.did.title
                  </ion-card-title>
                  <ion-card-subtitle>
                    form.did.subtitle
                  </ion-card-subtitle>
                </ion-card-header>
                <form-validate-submit custom-validation="true" form-id="did-form" input-selector="ion-card-content glass-input">
                  <div slot="fields">
                    <ion-card-content>
                      <glass-input has-end-slot="" input-id="did" input-name="did" label="form.did.input.label" label-position="stacked" lines="full" placeholder="form.did.input.placeholder" required="" value="">
                        <ion-buttons slot="end">
                          <ion-button slot="secondary">
                            <ion-icon name="clipboard" slot="icon-only"></ion-icon>
                          </ion-button>
                          <ion-button slot="primary">
                            <ion-icon name="qr-code" slot="icon-only"></ion-icon>
                          </ion-button>
                        </ion-buttons>
                      </glass-input>
                    </ion-card-content>
                  </div>
                  <div slot="buttons">
                    <div class="d-card-footer">
                      <glass-button color="primary" expand="block" fill="solid" label="form.buttons.proceed" type="submit"></glass-button>
                      <glass-button color="gray0" expand="block" label="form.buttons.clear" type="reset"></glass-button>
                    </div>
                  </div>
                </form-validate-submit>
              </ion-card>
            </div>
          </ion-content>
        </mock:shadow-root>
      </app-pdmfc-work-contract>
    `);
  });
});
