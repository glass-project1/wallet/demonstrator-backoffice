export { Components, JSX } from './components';

/**
 * Simple Distributed App code template, developed for integration with the wallet in mind.
 *
 * @module dapp-template
 */



export * from './utils';


/**
 * Constant whose value will be replaced by the build process by the version number
 *
 * @const DEMONSTRATOR_BACKOFFICE_CODE_VERSION
 * @category Constants
 */
export const DEMONSTRATOR_BACKOFFICE_CODE_VERSION = "##VERSION##"
