if (typeof require !== "undefined"){
  const opendsu = require("opendsu");
  if (!opendsu)
    throw new Error("Could not get opendsu");
  globalThis.opendsu = opendsu;
  console.log("OpenDSU Bound")
} else {
  console.error("OpenDsu was not defined beforehand! Have you imported the Bundles?")
}