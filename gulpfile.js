const {src, dest, parallel, series} = require('gulp');
const replace = require('gulp-replace');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if')
const merge = require('merge-stream');

const {argParser} = require('@glass-project1/dsu-utils/src/utils');
const glassPrefix = "@glass-project1/"

let {name, version} = require('./package.json');

if (name.indexOf(glassPrefix) !== -1)
  name = name.substr(glassPrefix.length).replaceAll("-", "_");

const STAGES = {
  BUILD: "build",
  DEPLOY: "deploy"
}

const MODES = {
  DEVELOPMENT: "development",
  PRODUCTION: "production"
}

const defaultOptions = {
  mode: MODES.DEVELOPMENT,
  stage: STAGES.BUILD,
  name: name,
  outputFolder: "dist/"
}

const config = argParser(defaultOptions, process.argv)

function isDev(){
  return config.mode === MODES.DEVELOPMENT;
}

function isProd(){
  return config.mode === MODES.PRODUCTION;
}

function isBuild(){
  return config.stage === STAGES.BUILD;
}

function isDeploy(){
  return config.stage === STAGES.DEPLOY;
}

function exportBlueprintDefault(){
  const tsProject = ts.createProject('tsconfig-blueprints.json', {
    target: "es5",
    module: "commonjs",
    declaration: true,
    declarationMap: true,
    emitDeclarationOnly: false,
    isolatedModules: false
  });

  const stream =  src('./blueprint/**/*.ts')
    .pipe(gulpIf(isDev(), sourcemaps.init()))
    .pipe(tsProject())

  return merge([
    stream.dts.pipe(dest("build")),
    stream.js.pipe(gulpIf(isProd(), uglify())).pipe(gulpIf(isDev(), sourcemaps.write())).pipe(dest("build"))
  ])
}


function fixImportsBuild (){
    const jsFiles = ['www/build/**/*.js']

    return src(jsFiles)
        .pipe(replace("##VERSION##", version))
        .pipe(replace(/import(:?[^{}]+? from)?\s?['"]opendsu['"];/g, function(match, p1, offset, string) {
            console.log(`Found an OpenDSU import in Bundle '${match}'. Removing...`)
            return ' '.repeat(match.length);
        }))
        .pipe(dest('www/build/'));
}

function fixImportsDist(){
    const jsFiles = ['dist/**/*.js']

    return src(jsFiles)
        .pipe(replace("##VERSION##", version))
        .pipe(replace(/import(:?[^{}]+? from)?\s?['"]opendsu['"];/g, function(match, p1, offset, string) {
            console.log(`Found an OpenDSU import in Bundle '${match}'. Removing...`)
            return ' '.repeat(match.length);
        }))
        .pipe(dest('dist/'));
}

function removeServiceWorkerScript(){
  const htmlFile  = ['www/index.html'];

  if (isDeploy())
    return src(htmlFile, {allowEmpty: true});

  return src(htmlFile, {allowEmpty: true}).pipe(replace(/<script>(.if\s?\('serviceWorker.+?)<\/script>/gms, function(match){
    console.log(`Found A dev ServiceWorker script. removing...`)
    return '<script>console.log("The Stencil dev service worker script has been removed")</script>';
  })).pipe(dest('www/'))
}

exports.default = series(fixImportsBuild, fixImportsDist, removeServiceWorkerScript)
