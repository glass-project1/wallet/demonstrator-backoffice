#!/bin/bash

echo "Patching network and country configs to schema in folder $(pwd) with env preffix ${ENV_PREFFIX}";

node ./node_modules/@glass-project1/dsu-utils/src/streamAndReplace.js --srcPath="./docker/config" --paramIdentifier='%' --destPath="./apihub-root/external-volume/" --baseFolderReplacement="config" %ENV_PREFFIX="$ENV_PREFFIX" %ENV_SUFFIX="$ENV_SUFFIX_FOR_REPLACE" %ADAPTER_ENDPOINT="$ADAPTER_ENDPOINT" %MIDDLEWARE="$GlassMiddleware" %COUNTRY_CODE="$COUNTRY_CODE"

echo "Result:";

cat ./apihub-root/external-volume/config/apihub.json

cd privatesky

echo "Booting server from $(pwd)";

node ./psknode/bin/scripts/apiHubLauncher.js --jsondb='./../db'