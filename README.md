[![Banner](https://static.wixstatic.com/media/2844e6_eb52a8ed1a5249eb87ddc015c7be7ce2~mv2.jpg/v1/fill/w_438,h_156,al_c,q_80,usm_0.66_1.00_0.01/2021-01-21_11-35-06.webp)](https://www.glass-h2020.eu/)

#### Status
[![Build](https://gitlab.com/glass-project1/wallet/demonstrator-backoffice/badges/master/pipeline.svg)](http://www.pdmfc.com)

![coverage](https://gitlab.com/glass-project1/wallet/demonstrator-backoffice/badges/main/coverage.svg?job=coverage)



## Demonstrator-Backoffice

This repository acts as the demonstrator to interact and experience the full functionallity of the Glass-Wallet.

Node versions are documented in glass wallet workspace, but at the time of this readme are:
- Node = 16;
- NPM >= 8.0.0; (we have not fully tested with npm above 8.10 but in principle it shouldn't matter)

### Installation

In order to use the demonstrator-backoffice you need to follow a list of steps presented below:

#### Step 1: Installation

Run ```npm install``` - installs the necessary dependencies

#### Step 2: Developing

This project is divided in two parts the backend and frontend. The frontend is a stencil/ionic project, the source code is under the ```src``` folder. For more details on how to develop Stencil Applications, 
please visit the [stencil website](https://stenciljs.com/).  
The backend is a typescript project that contains small middlewares that will run on the apiHub server, the source code for the backend is located under the ```backoffice/src```.
<!-- 

variables 
MARKET_DOMAIN=glass.local GlassMiddleware=moj GLASS_COUNTRY_CODE=pt LOCALE_KEY=pt npm run server

#### Step 3: Building -->

<!-- This application is set up so it can be built under an ```OpenDSU Workspace``` (examples can be found [here](https://gitlab.com/glass-project1/wallet/templates/opendsu-workspace-template), [here](https://gitlab.com/glass-project1/wallet/glass-wallet-workspace), [here](https://gitlab.com/glass-project1/wallet/workspaces/dsu-explorer-workspace), and [here](https://gitlab.com/glass-project1/wallet/dsu-blueprint-workspace)).

To build and test it in a standalone fashion (the same way that is done in CI):

```npm run test:ci -- --command="npm run build:prod"``` - This will:
 - emulate an OpenDSU workspace under the current repository:
   - Add an ```apihub-root``` folder, complete with all the necessary configuration files;
   - add some dependencies and new scripts to ```package.json``` (one of them is OpenDSU's taskrunner ```octopus```);
   - start the OpenDSU Apihub (equivalent to the ```npm run server``` command)
   - add an ```octopus``` configuration file ```octopus-freeze.json``` than contains the tasks necessary for:
     - Cloning the version controlled OpenDSU repository and perform the necessary build procedures;
     - Cloning the ```blueprint-loader``` repository, building it and deploy it to the apihub, allowing you to test this template application in the browser;
 - re-run ```npm install```, which will this time also install the dependencies in the ```octopus-freeze``` file
 - start the OpenDSU ApiHub server (basically run ```npm run server```)
 - perform the build command ```npm run build-all``` that will instruct ```octopus``` to run its config file ```octopus-freeze.json```;
 - finally, run the ```npm run build:prod``` command which will pack you application under the ```www``` folder;
 - ***IMPORTANT*** the ci command adds several folders and modifies several files (like package.json and stencil.config.json). ***These changes should not be committed!!***

***Note:*** After the last command, since it's designed to run in CI, some lingering processes might remain that can impede the running of the server.
To prevent that locally you can ```kill -9 $(pgrep node)``` in linux. In windows all the lingering node processes must be stopped -->
<!-- 
#### Step 3.1: Rebuilding the Application
 
After you have the ```npm run test:ci``` command (and killed any lingering processes), to rebuild the application, the steps are easier:
 - manually start the OpenDSU ApiHub server: ```npm run server```;
 - on a separate terminal:
   - build the application: ```npm run build:prod``` or ```npm run build``` (the difference in this case lies in Stencil's build process. build is for the dev version while prod packs the application in a minified and optimized fashion). This will rebuild your ```www``` folder;
   
#### Step 4: Packing the Application in DSU format

##### For the first time

If you ran the ```npm run test:ci -- --command="npm run npm run build:prod"``` above, then the application will already have been packed in DSU format:
 - you will find a ```seed``` file under the root folder containing the KeySSI for the DSU where the Application code is stored;
 - under ```./apihub-root/dapp-template/loader``` you will find the code to a simple loader web app, that can register new instances of the ```dapp-template``` and launch them via a simple register/login procedure. it will be available under ```localhost:8080/dapp-template/loader```;
 - under ```./apihub-root/dapp-template/wallet-patch``` you will find a ```seed``` file, which will contain the same KeySSI as the previous ```seed``` file

To replicate this 'manually', make sure the server is running and run ```npm run build-all```

##### For subsequent builds

 - make sure the server is running;
 - make sure you have already built your application and have a ```www``` folder, or rebuild it via the ```npm run build``` or ```npm run build:prod``` commands
 - run the command ```npm run build:dsu```. You should be an output containing the generated KeySSI;
 - Since this is an update, the same DSU will be used, and any subsequent login to an already existing dapp instance will automatically display the updated code. (since we use service workers, for development, it's usually better to use incognito window, and close/reopen them in between builds)

#### Technical Details

##### Octopus

Octopus is a taskrunner, that reads from a json file ```octopus-freeze.json``` and executes the commands defined in that file.

For example, when you run ```npm install``` it will also execute all the commands in the ```dependencies``` entry. And when you run ```npm run build-all``` it will executed the commands in the ```build``` entry

##### Blueprints

Under the ```./blueprint``` folder you will find a ```build.ts``` file containing the definition to a Type script class. This very simple class has the definition on how to create the DSU.
In this case:

```ts
@DSUBlueprint(undefined)                                                    // <-- Defines the class as a DSU, undefined means hte domain where it is build can be selected by the build process, and the KeySSI type default to SeedSSI
export default class WalletBuildBlueprint extends DBModel {

    @addFolderFS('www', '/')                                                // <-- Instructs the dsu-bluprint library to copy the contents of the www folder onto the root of the dsu
    code?: any = undefined;

    constructor(blueprint?: WalletBuildBlueprint | {}) {
        super();
        constructFromObject<WalletBuildBlueprint>(this, blueprint);
    }
}
```

The ```dsu-blueprint``` library is the CORE component to all the functionalities we are presenting. For more details please review the documentation and tests. We use them for:
 - Simple DSU definition: (files, folders, simple data);
 - validation: there are several validation decorators you can apply (and create new ones)
 - rendering: instructing a model on how to render itself (you can see an example of this in the ```glass-loader```)

When you execute ```npm run build``` or ```npm run build:prod``` these blueprints will be transpiled and exported to the `./build` folder to be later used by the ```npm run build:dsu``` command;

##### Stencil

The ```npm run build``` or ```npm run build:prod``` commands trigger Stencil's build process. The definitions for which can be found under ```stencil.config.js```.

Most of this file is boilerplate, but here are some notable points:
 - namespace: controls the name of the output files (these must match the ones in package.json);
 - globalStyle and globalScript: self-descriptive. global script will be executed on boot of the application, global styles sets a global style for the application;
 - rollupPlugins: 
   - before: We have a trigger to run gulp. more on this in the next chapter;
   - after: nodePolyFills: we need this for the wallet because we use the Buffer 'node' object for encryption and we need it polyfilled. You might not need this.
 - outputTargets: the one that interests us is the 'www'. notice how we copy the required dependencies to the www folder during build.

###### Testing

The testing follows stencil test procedures, eg: ts files containing the keywords 'spec' and 'e2e'.

To these stencil specific keywords we added the 'integration' keyword, that will be ran with open dsu in its environment.

This allows for integration testing with any wallet/opendsu features

##### Gulp

In conjunction with stencil's build process we use gulp to:
 - transpile the blueprints to standard javascript;
 - remove the hardcoded OpenDSU requires stencil leaves behind, that would result in an error. This dependency is later injected by our loader code;
 - hardcode the current version onto the code
 - remove a service worker script that stencil leaves behind in dev builds that conflicts with ours; -->





### Repository Structure

```
demonstrator-backoffice
│
│   .gitignore                  <-- Defines files ignored to git
│   .gitlab-ci.yml              <-- GitLab CI/CD config file
│   .nmpignore                  <-- Defines files ignored by npm
│   .nmprc                      <-- Defines the Npm registry for this package
│   gulpfile.js                 <-- Gulp build scripts. used to clean the stencil output bundles from leftover opendsu imports. also transpiles the blueprint files
│   jsdocs.json                 <-- Documentation generation configuration file
│   LICENCE.md                  <-- Licence disclamer
│   nodemon.json                <-- Nodemon config file (allows to live test ts files)
│   package.json    
│   package-lock.json   
│   README.md                   <-- Readme File dynamically compiled from 'workdocs' via the 'docs' npm script
│   stencil.config.js           <-- Stencil configuration file
│   tsconfig.json               <-- Typescript config file. Is overriden in 'gulpfile.js' 
│   tsconfig-blueprints.json    <-- Typescript config file for blueprint transpilation
│   
└───bin 
│   │   tag_release.sh          <-- Script to help with releases
│       
└───docs    
│   │   ...                     <-- Dinamically generated folder, containing the compiled documentation for this repository. generated via the 'docs' npm script
│       
└───src 
│   │   ...                     <-- Source code for this repository
│       
└───workdocs                    <-- Folder with all pre-compiled documentation
│    │   ...    
│    │   Readme.md              <-- Entry point to the README.md   
│   
└───www 
     |  ...                     <-- Dinamically generated folder containing the actual code that should go into the loader folder
```

<!-- ### Creating releases and Publishing to the DApp Market

#### Releases

under this repository (for linux and macOS), you can run ```npm run release``` and a small script will:
 - show you all existing tags
 - ask you to choose and confirm a new tag
 - ask you to choose and confirm a tagging message
 - run `npm run build:prod`
 - run `npm run test`
 - create the tag, and push it to the remote repository

***NOTE:*** the tag creation and pushing is not affected by the results of the build process or the tests.
This is because, since the actual building happens in CI, we sometimes have situations where we want to continue the process regardless of these results locally

Optionally you can do it manually by simply creating a tag and pushing it to the remote

#### Publishing

##### To NPM package registry

Our CI is programmed to, whenever you create a tag in the master branch, automatically run the build process and publish is to NPM.


notice the publishing config entry in your package.json. The project ID must the one for the repository you are using. you can find it under the repository name in gitlab's page

```
  "publishConfig": {
    "@glass-project1:registry": " https://gitlab.com/api/v4/projects/890/packages/npm/"
  },
```

##### To the DApp Market

While we do our development in dedicated branches that are merged to the ```master``` branch, we also maintain 2 special branches in our repositories:
 - ```dev```: controls our dev release;
 - ```tst```: controls ours tst release

DApp publishing is set to automatically happen when we push to these branches via CI


###### via CI

Apart from local development and testing, we rely heavily on CI/CD for certain operations:
 - Continuous testing of all repositories;
 - Automatic library release to the gitlab npm registry;
 - Automatic docker build and publish to the gitlab docker registry;
 - Automatic DApp publishing to the Glass DApp Market;

For the sake of this readme, only the latter is relevant.

The ```.gitlab-ci.yml``` file defines certain operations:

 - before_script: 
   - updates os dependencies, install the necessary ones to run and test stencil apps and caches them;
   - installs npm dependencies from package-lock.json and caches them;
 - tests: basically runs the command ```npm run test:ci -- --command="npm run test:all"``` which runs all tests with open dsu in the process to allow for integrations tests
 - build: basically runs the build:prod command;
 - npm-deploy: publishes the package to npm when a tag is created
 - dsu-publish: builds the application and publishes it to the running DApp market on pdm servers ***if the branch is 'dev' or 'tst'***. This allows us to work in the master branch, tag the master branch, and only merge those tags to dev or tst branches, triggering the dsu publication

###### Manually

Having the server running and after having built your application (including the dsu) run ```npm run publish:dsu -- --pathToOpenDSU='./privatesky/psknode/bundles/openDSU.js'```;

this will publish the app in you running local instance of the dapp-market (domain is 'glass.local') you can change any of the settings by overriding the parameters passed in the original npm script

###### Automatic publish on environment reset

Since we are in development, sometimes it will be required to reset the dev and tst environments.

When that happens we have set up our ```glass-wallet-workspace``` CI in a way we can quickly re-publish all the dapps required for the wallet to fully function.

This translates to a CI script found under the ```glass-wallet-workspace``` repository you can find [here](https://gitlab.com/glass-project1/wallet/glass-wallet-workspace/-/blob/master/dapp-publish.yml)

If you want your dapp to be automatically published when we reset our environments, please do a Pull Request adding your dapp to this file, and give the PDM&FC team permissions to trigger your repository's CI -->

<!-- ### Glass Integration

#### Dependencies
    "@glass-project1/logging": "latest",
    "@glass-project1/decorator-validation": "latest",
    "@glass-project1/db-decorators": "latest",
    "@glass-project1/opendsu-types": "latest",
    "@glass-project1/dsu-blueprint": "latest",
    "@glass-project1/ui-decorators": "latest",         
    "@glass-project1/glass-toolkit": "latest",
    "@glass-project1/native-integration": "latest" <-- only needed if you require acces to native features (camera, localization, etc)

The above dependencies are 'boilerplate' for glass related integrations (when regarding the wallet and opendsu)

    "@glass-project1/dsu-utils": "latest",

This is used by the build scripts and environment setup

    "@glass-project1/base-web-components": "latest",   <-- common use web components
    "@glass-project1/localization": "latest",          <-- UI localization lib  
    "@glass-project1/ui-decorators-web": "latest",     <-- adds rendering strategies to blueprints (eg: the model can render itself in HTML5) 

These are UI dependencies, which you can use or not. (if you do, you'll also need ```@ionic/core```)

#### Wallet API interactions

For the most part, all the objects you need to interact with your instance of the DApp (database, did, etc) are supplied in ```glass-toolkit```. 
Please refer to the [documentation](https://glass-project1.gitlabpages.ubitech.eu/wallet/glass-toolkit/) 

##### Updating glass related dependencies in bulk

We use package-lock.json to freeze our dependencies. To update them in bulk the recommended procedure is to delete the ```@glass-project1``` folder from the ```node_modules```
as well as ```package-lock.json``` and run ```npm install```. This way all no glass dependencies will remain the same.

#### Warnings during build

If you are using ```@glass-project1/base-web-components``` and/or ```@glass-project1/native-integration``` then the following warnings might show up during stencils build process:

```
[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/clipboard' is imported by
           ./node_modules/@glass-project1/native-integration/lib/clipboard/clipboard.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/filesystem' is imported by
           ./node_modules/@glass-project1/native-integration/lib/fs-storage/FsStorage.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor-community/camera-preview' is imported by
           ./node_modules/@glass-project1/native-integration/lib/camera/camera.js, but could not be resolved – treating
           it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/device' is imported by ./node_modules/@glass-project1/native-integration/lib/device/device.js,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/geolocation' is imported by
           ./node_modules/@glass-project1/native-integration/lib/location/location.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/storage' is imported by
           ./node_modules/@glass-project1/native-integration/lib/light-storage/storage.js, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/haptics' is imported by ./node_modules/@glass-project1/native-integration/lib/haptics/haptics.js,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/clipboard' is imported by @capacitor/clipboard?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/filesystem' is imported by @capacitor/filesystem?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor-community/camera-preview' is imported by @capacitor-community/camera-preview?commonjs-external,
           but could not be resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/device' is imported by @capacitor/device?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/geolocation' is imported by @capacitor/geolocation?commonjs-external, but could not be resolved
           – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/storage' is imported by @capacitor/storage?commonjs-external, but could not be resolved –
           treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           '@capacitor/haptics' is imported by @capacitor/haptics?commonjs-external, but could not be resolved –
           treating it as an external dependency
```

That is ok. These dependencies will be provided to you by the wallet/loader. Since they'll require native code, they have to be builtin the loader. They cannot be loaded at runtime as per app store regulations;

You will also have these warnings:

```
[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           'opendsu' is imported by ./node_modules/@glass-project1/opendsu-types/lib/opendsu.js, but could not be
           resolved – treating it as an external dependency

[ WARN  ]  Bundling Warning UNRESOLVED_IMPORT
           'opendsu' is imported by opendsu?commonjs-external, but could not be resolved – treating it as an external
           dependency
```

This is also ok. OpenDSU will be injected into you DApp by the wallet/loader. Since it is not available on npm, it does not follow normal dependency rules



 -->




### Repository Languages

![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![NodeJS](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![ShellScript](https://img.shields.io/badge/Shell_Script-121011?style=for-the-badge&logo=gnu-bash&logoColor=white)




### Social

[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://pt.linkedin.com/company/pdmfc)


#### Disclaimer:

![Disclamer](https://static.wixstatic.com/media/2844e6_69acaab42d5a47c9a20a187b384741ef~mv2.png/v1/fill/w_531,h_65,al_c,q_85,usm_0.66_1.00_0.01/2021-01-21_11-27-05_edited.webp)
