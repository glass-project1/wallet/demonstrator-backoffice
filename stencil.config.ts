import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import execute from 'rollup-plugin-execute';
import nodePolyfills from 'rollup-plugin-node-polyfills'


export const config: Config = {
  namespace: 'demonstratorbackoffice',
  globalStyle: 'src/global/app.scss',
  globalScript: 'src/global/app.ts',
  sourceMap: true,
  testing: {
    /**
     * Gitlab CI doesn't allow sandbox, therefor this parameters must be passed to your Headless Chrome
     * before it can run your tests
     */
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox'],
    browserHeadless: true,
    transform: {
      '^.+\\.(ts|tsx|js|jsx|css)$': "<rootDir>/node_modules/@stencil/core/testing/jest-preprocessor"
    },
    transformIgnorePatterns: ["/node_modules/.*"],
    verbose: true,
    collectCoverage: true,
    coverageDirectory: "./workdocs/coverage",
    coverageReporters: [
      "json-summary",
      "text-summary",
      "text",
      "html"
    ],
    reporters: [
      "default",
      ["jest-junit", {outputDirectory: './workdocs/coverage', outputName: "junit-report.xml"}]
    ]
  },
  rollupPlugins: {
    before: [
      execute('gulp')
    ],
    after: [
      nodePolyfills()
    ]
  },
  outputTargets: [
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      baseUrl: 'https:/demonstrator-backoffice.local/',
      copy: [
        // Base Web Components Assets
        {src: '../node_modules/@glass-project1/base-web-components/assets/fonts', dest: 'assets/fonts', warn: true},
        {src: '../node_modules/@glass-project1/base-web-components/assets/images', dest: 'assets/images', warn: true},
        {src: '../assets/locale', dest: 'assets/locale', warn: true},
        {src: '../assets/icon', dest: 'assets/icon', warn: true},
        // PrivateSky bundles TODO: Refactor sw to not need this. inherit from loader and avoid loading files twice
        {src: '../privatesky/psknode/bundles/webshims.js', dest: 'constitution/webshims.js', warn: true},
        {src: '../privatesky/psknode/bundles/pskruntime.js', dest: 'constitution/pskruntime.js', warn: true},
        {src: '../privatesky/psknode/bundles/loaderBoot.js', dest: 'constitution/loaderBoot.js'},
        {src: '../patch/bootstrap.js', dest: 'bootstrap.js', warn: true},
        {src: '../assets/endpoints', dest: 'assets/endpoints', warn: true},
      ]
    }
  ],
  plugins: [
    sass()
  ]
};
